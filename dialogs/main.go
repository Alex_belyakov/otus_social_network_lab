package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	httpProcessedRequests = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "api_http_requests_total",
		Help: "The total number of api http requests",
	}, []string{"path"})
	httpDuration = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name: "api_http_duration_seconds",
		Help: "Duration of HTTP requests.",
	}, []string{"path"})
	sentMessages = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "api_sent_messages",
		Help: "Sent messages by users.",
	}, []string{"user"})
	userErrors = promauto.NewCounter(prometheus.CounterOpts{
		Name: "api_user_errors",
		Help: "Amount of api errors affecting user experience.",
	})
)

var logger Logger

type MessageModel struct {
	Text string `json:"text"`
}

func prometheusMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		path := c.Request.URL.Path
		timer := prometheus.NewTimer(httpDuration.WithLabelValues(path))
		c.Next()

		httpProcessedRequests.WithLabelValues(path).Inc()

		timer.ObserveDuration()
	}
}

func contextMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				log.Println("panic occurred:", err)
				userErrors.Inc()
			}

			if len(c.Errors) > 0 || c.Writer.Status() == http.StatusInternalServerError {
				userErrors.Inc()
			}
		}()
		var contextUid = c.Request.Header.Get("Request-Uid")

		logger = Logger{ContextUid: contextUid}
		c.Next()
	}
}

func sendMessageInternal(c *gin.Context) {
	secretKey, exists := os.LookupEnv("secret_key")

	if !exists {
		fmt.Println("secret_key env not found")
		c.IndentedJSON(http.StatusInternalServerError, nil)
		return
	}

	var key = c.Request.Header.Get("Secret-Key")

	if key != secretKey {
		c.IndentedJSON(http.StatusNotFound, nil)
		return
	}

	idParam := c.Param("userId")
	userId, err := strconv.Atoi(idParam)
	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "incorrect user id"})
		return
	}

	friendParam := c.Param("friendId")
	friendId, err := strconv.Atoi(friendParam)
	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "incorrect friend id"})
		return
	}

	sendMessage(userId, friendId, c)
}

func markMessageRead(c *gin.Context) {
	token, err := c.Cookie("token")

	if err != nil {
		log.Println("token not set")
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	userId, err := getUserId(token)

	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	idParam := c.Param("messageId")
	messageId, err := strconv.Atoi(idParam)
	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "incorrect message id"})
		return
	}

	if err := dbReadMessage(c.Request.Context(), userId, messageId); err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "read error"})
		return
	}
}

func listMessagesInternal(c *gin.Context) {
	secretKey, exists := os.LookupEnv("secret_key")

	if !exists {
		fmt.Println("secret_key env not found")
		c.IndentedJSON(http.StatusInternalServerError, nil)
		return
	}

	var key = c.Request.Header.Get("Secret-Key")

	if key != secretKey {
		c.IndentedJSON(http.StatusNotFound, nil)
		return
	}

	idParam := c.Param("userId")
	userId, err := strconv.Atoi(idParam)
	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "incorrect user id"})
		return
	}

	friendParam := c.Param("friendId")
	friendId, err := strconv.Atoi(friendParam)
	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "incorrect friend id"})
		return
	}

	listMessages(userId, friendId, c)
}

func sendMessageExternal(c *gin.Context) {
	token, err := c.Cookie("token")

	if err != nil {
		log.Println("token not set")
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	userId, err := getUserId(token)

	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	idParam := c.Param("friendId")
	friendId, err := strconv.Atoi(idParam)
	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "incorrect friend id"})
		return
	}

	sendMessage(userId, friendId, c)
}

func listMessagesExternal(c *gin.Context) {
	token, err := c.Cookie("token")

	if err != nil {
		log.Println("token not set")
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	userId, err := getUserId(token)

	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	idParam := c.Param("friendId")
	friendId, err := strconv.Atoi(idParam)
	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "incorrect friend id"})
		return
	}

	listMessages(userId, friendId, c)
}

func sendMessage(userId int, friendId int, c *gin.Context) {
	var (
		msgId   int
		err     error
		message MessageModel
	)

	if userId == friendId {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "sender and receiver cannot be equal"})
		return
	}

	if err := c.BindJSON(&message); err != nil {
		log.Print("model error")
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "incorrect body"})
		return
	}

	logger.logInfo(fmt.Sprintf("sending the message for userId: %d friendId: %d", userId, friendId))

	storage, _ := os.LookupEnv("storage")

	switch storage {
	case "postgresql":
		log.Println("working with postgresql")
		msgId, err = dbSendMessage(c.Request.Context(), userId, friendId, message.Text)
	case "redis":
		log.Println("working with redis")
		msgId, err = redisSendMessage(c.Request.Context(), userId, friendId, message.Text)
	default:
		log.Println("working with postgresql")
		msgId, err = dbSendMessage(c.Request.Context(), userId, friendId, message.Text)
	}

	if err != nil {
		logger.logError(fmt.Sprintf("sending the message for userId: %d friendId: %d failed", userId, friendId), err)
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "send message failure, please try again"})
		return
	}

	logger.logInfo(fmt.Sprintf("the message for userId: %d friendId: %d sent", userId, friendId))

	sentMessages.WithLabelValues(fmt.Sprint(userId)).Inc()

	c.IndentedJSON(http.StatusOK, gin.H{"id": msgId})
}

func listMessages(userId int, friendId int, c *gin.Context) {
	var (
		dialog Dialog
		err    error
	)

	if userId == friendId {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "sender and receiver cannot be equal"})
		return
	}

	count, err := strconv.Atoi(c.DefaultQuery("count", "100"))
	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "incorrect count"})
		return
	}

	offset, err := strconv.Atoi(c.DefaultQuery("offset", "0"))
	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "incorrect offset"})
		return
	}

	storage, _ := os.LookupEnv("storage")

	switch storage {
	case "postgresql":
		log.Println("working with postgresql")
		dialog, err = dbListDialog(userId, friendId, offset, count)
	case "redis":
		log.Println("working with redis")
		dialog, err = redisListDialog(c.Request.Context(), userId, friendId, offset, count)
	default:
		log.Println("working with postgresql")
		dialog, err = dbListDialog(userId, friendId, offset, count)
	}

	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "get dialog failure, please try again"})
		return
	}

	c.IndentedJSON(http.StatusOK, dialog)
}

func init() {
	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}

	prometheus.Register(httpProcessedRequests)
	prometheus.Register(httpDuration)
	dbInit()
}

func route() {
	defer func() {
		if recover() != nil {
			log.Println("panic recovered")
		}
	}()

	router := gin.Default()

	router.Use(prometheusMiddleware())
	router.Use(contextMiddleware())

	router.GET("/metrics", gin.WrapH(promhttp.Handler()))
	router.POST("/dialog/:friendId/send", sendMessageExternal)
	router.GET("/dialog/:friendId/list", listMessagesExternal)
	router.POST("/dialog/message/:messageId/read", markMessageRead)
	// internal methods
	router.POST("/dialog/from/:userId/to/:friendId", sendMessageInternal)
	router.GET("/dialog/from/:userId/to/:friendId", listMessagesInternal)

	router.Run(":80")
}

func main() {
	ctx := context.Background()
	exitCtx, exitFunc := context.WithCancel(ctx)
	defer conn.Close()
	defer exitFunc()

	if err := startEventsResponseListener(exitCtx); err != nil {
		panic(err)
	}

	if err := startEventsSender(exitCtx); err != nil {
		panic(err)
	}

	route()
}
