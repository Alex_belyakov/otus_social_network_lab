package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"sync"
	"time"

	_ "github.com/lib/pq"
)

type Dialog struct {
	Id       int       `json:"id"`
	FriendId int       `json:"friendId"`
	Messages []Message `json:"messages"`
}

type Message struct {
	Id   int    `json:"id"`
	Text string `json:"text"`
}

var (
	once sync.Once
	conn *sql.DB
)

func dbGetUnansweredEvents(offset int, count int) (events []EventData, err error) {
	db := GetConnection()

	rows, err := db.Query(`
		select id, dialogId, body
		from events
		where received = false and sent = true and expireat < $1
		offset $2 limit $3;`, time.Now(), offset, count)

	if err != nil {
		fmt.Println("query error", err)
		err = errors.New("query error")
		return
	}

	defer rows.Close()

	for rows.Next() {
		var event EventData

		err = rows.Scan(&event.Id, &event.DialogId, &event.Event)

		if err != nil {
			fmt.Println("error reading events", err)
			return
		}

		events = append(events, event)
	}

	err = rows.Err()
	if err != nil {
		fmt.Println("error reading events", err)
	}

	return
}

func dbMarkSentEvent(ctx context.Context, eventId int, timeoutSeconds int) error {
	db := GetConnection()
	expireAt := time.Now().Add(time.Second * time.Duration(timeoutSeconds))

	if _, err := db.ExecContext(ctx,
		`update events
		set sent = true, expireat = $1
		where id = $2;`, expireAt, eventId); err != nil {
		log.Println("failed when updating events", err)
		return err
	}

	return nil
}

func dbMarkReceivedEvent(eventId int) error {
	db := GetConnection()

	if _, err := db.Exec(
		`update events
		set received = true
		where id = $1;`, eventId); err != nil {
		log.Println("failed when updating events", err)
		return err
	}

	return nil
}

func dbGetEvents(offset int, count int) (events []EventData, err error) {
	db := GetConnection()

	rows, err := db.Query(`
		select id, dialogId, body
		from events
		where sent = false
		order by id
		limit $1 offset $2;`, count, offset)

	if err != nil {
		fmt.Println("query error", err)
		err = errors.New("query error")
		return
	}

	defer rows.Close()

	for rows.Next() {
		var event EventData

		err = rows.Scan(&event.Id, &event.DialogId, &event.Event)

		if err != nil {
			fmt.Println("error reading events", err)
			return
		}

		events = append(events, event)
	}

	err = rows.Err()
	if err != nil {
		fmt.Println("error reading events", err)
	}

	return
}

func dbListDialog(userId int, friendId int, offset int, count int) (dialog Dialog, err error) {
	db := GetConnection()

	dialog = Dialog{FriendId: friendId}

	rows, err := db.Query(`
		select m.id, text, snd.dialogId
		from messages as m
		join userDialogs as snd on snd.id = m.senderDialogId and snd.userId = $1
		join userDialogs as rcv on snd.dialogId = rcv.dialogId and rcv.userId = $2
		order by id desc
		limit $3 offset $4;`, userId, friendId, count, offset)

	if err != nil {
		fmt.Println("query error", err)
		err = errors.New("query error")
		return
	}

	defer rows.Close()

	for rows.Next() {
		var message Message

		err = rows.Scan(&message.Id, &message.Text, &dialog.Id)

		if err != nil {
			fmt.Println("error reading friends", err)
			return
		}

		dialog.Messages = append(dialog.Messages, message)
	}

	err = rows.Err()
	if err != nil {
		fmt.Println("error reading user posts", err)
	}

	return
}

func dbSendMessage(ctx context.Context, userId int, friendId int, text string) (msgId int, err error) {
	db := GetConnection()
	tx, err := db.BeginTx(ctx, &sql.TxOptions{Isolation: sql.LevelSerializable})

	if err != nil {
		log.Println("transaction begin failed", err)
		return
	}

	defer tx.Rollback()
	log.Println("transaction begun", err)

	now := time.Now()
	var dialogId int
	var senderDialogId int

	row := tx.QueryRow(`
		select snd.dialogId 
		from userDialogs as snd 
		join userDialogs as rcv on rcv.dialogId = snd.dialogId 
		where snd.userId = $1 and rcv.userId = $2 limit 1`,
		userId, friendId)
	switch err = row.Scan(&dialogId); err {
	case sql.ErrNoRows:
		result := tx.QueryRowContext(ctx,
			`insert into dialogs
			default values returning id;`)

		if err = result.Scan(&dialogId); err != nil {
			log.Println("create dialog failed", err)
			return 0, err
		}

		result = tx.QueryRowContext(ctx,
			`insert into userDialogs(
				userid, dialogid)
				values ($1, $2) returning id;`,
			userId, dialogId)

		if err = result.Scan(&senderDialogId); err != nil {
			log.Println("create dialog failed", err)
			return 0, err
		}

		if _, err := tx.ExecContext(ctx,
			`insert into userDialogs(
				userid, dialogid)
				values ($1, $2);`,
			friendId, dialogId); err != nil {
			log.Println("create dialog failed", err)
			return 0, err
		}
	case nil:
		result := tx.QueryRowContext(ctx,
			`select id
			from userDialogs
			where userid = $1 and dialogid = $2
			limit 1;`,
			userId, dialogId)

		if err = result.Scan(&senderDialogId); err != nil {
			log.Println("create dialog failed", err)
			return 0, err
		}
	default:
		log.Println("create dialog id query error", err)
		return
	}
	log.Println("got the dialog", err)

	result := tx.QueryRowContext(ctx,
		`insert into messages(
		senderdialogid, text, createdat)
		values ($1, $2, $3) returning id;`,
		senderDialogId, text, now)

	err = result.Scan(&msgId)

	if err != nil {
		log.Println("create message failed", err)
		return
	}

	log.Println("inserted the message", err)

	var eventId int

	if err != nil {
		log.Println("Error unmarshaling JSON:", err)
		return
	}

	result = tx.QueryRowContext(ctx,
		`insert into events(
		dialogid, body, createdat, sent, received)
		values ($1, $2, $3, $4, $5) returning id;`,
		dialogId, ``, now, false, false)

	if err = result.Scan(&eventId); err != nil {
		log.Println("create event failed", err)
		return
	}

	event := Event{Id: eventId, Type: sendEvent, SenderId: userId, ReceiverId: friendId, DialogId: dialogId, Timestamp: now, MessageIds: []int{msgId}}
	bytes, err := json.Marshal(event)

	if _, err = tx.ExecContext(ctx,
		`update events
		set body = $1
		where id = $2;`,
		string(bytes), eventId); err != nil {
		log.Println("create event failed", err)
		return 0, err
	}

	if err = tx.Commit(); err != nil {
		fmt.Println("commit failed", err)
		return
	}
	log.Println("transaction commited", err)

	return
}

func dbReadMessage(ctx context.Context, receiverId int, messageId int) (err error) {
	db := GetConnection()
	tx, err := db.BeginTx(ctx, &sql.TxOptions{Isolation: sql.LevelReadCommitted})

	if err != nil {
		log.Println("transaction begin failed", err)
		return
	}

	defer tx.Rollback()
	log.Println("transaction begun", err)

	var eventId int
	var dialogId int
	var senderId int
	var messageIds []int

	rows, err := tx.QueryContext(ctx,
		`update messages as upd
		set read = true
		from messages as m
		join userDialogs as snd on m.senderDialogId = snd.id
		join userDialogs as rcv on snd.dialogId = rcv.dialogId and rcv.UserId = $1
		where m.id <= $2 and m.read = false and m.id = upd.id
		returning upd.id, snd.dialogId, snd.userId;`, receiverId, messageId)

	if err != nil {
		fmt.Println("query error", err)
		err = errors.New("query error")
		return
	}

	defer rows.Close()

	for rows.Next() {
		var messageId int

		err = rows.Scan(&messageId, &dialogId, &senderId)

		if err != nil {
			fmt.Println("error reading messages", err)
			return
		}

		messageIds = append(messageIds, messageId)
	}

	if len(messageIds) == 0 {
		return
	}

	now := time.Now()
	result := tx.QueryRowContext(ctx,
		`insert into events(
		dialogid, body, createdat, sent, received)
		values ($1, $2, $3, $4, $5) returning id;`,
		dialogId, ``, now, false, false)

	if err = result.Scan(&eventId); err != nil {
		log.Println("create event failed", err)
		return
	}

	event := Event{Id: eventId, Type: readEvent, SenderId: senderId, ReceiverId: receiverId, DialogId: dialogId, Timestamp: now, MessageIds: messageIds}
	bytes, err := json.Marshal(event)

	if _, err = tx.ExecContext(ctx,
		`update events
		set body = $1
		where id = $2;`, string(bytes), eventId); err != nil {
		log.Println("create event failed", err)
		return
	}

	if err = tx.Commit(); err != nil {
		fmt.Println("commit failed", err)
		return
	}
	log.Println("transaction commited", err)

	return
}

func GetConnection() *sql.DB {
	once.Do(func() {
		var err error

		connStr, exists := os.LookupEnv("social_network_db")

		if !exists {
			log.Fatal("social_network_db env not found")
		}

		if conn, err = sql.Open("postgres", connStr); err != nil {
			log.Fatal(err, "error getting the db connection")
		}
		conn.SetMaxOpenConns(20)
		conn.SetMaxIdleConns(20)
		conn.SetConnMaxLifetime(time.Minute)
	})
	return conn
}

func dbInit() {
	postgresStr, exists := os.LookupEnv("postgres_db")

	if !exists {
		log.Fatal("postgres_db env not found")
	}

	postgresDb, err := sql.Open("postgres", postgresStr)

	if err != nil {
		fmt.Println(err)
		log.Fatal("init connection failed")
	}

	defer postgresDb.Close()

	row := postgresDb.QueryRow("SELECT 1 FROM pg_catalog.pg_database WHERE datname='otus_social_network' limit 1")

	switch row.Err() {
	case sql.ErrNoRows:
		_, err = postgresDb.Exec(`CREATE DATABASE otus_social_network`)
		if err != nil {
			fmt.Println(err)
			log.Fatal("create db failed")
		}
	case nil:
	default:
		log.Println("get user id by token query error", err)
		return
	}

	db := GetConnection()

	_, err = db.Exec(`
		CREATE TABLE IF NOT EXISTS public.userDialogs
		(
			id serial NOT NULL,
			userId integer NOT NULL,
			dialogId integer NOT NULL,
			CONSTRAINT pk_user_dialogs PRIMARY KEY (id)
		);

		CREATE UNIQUE INDEX IF NOT EXISTS idx_dialog_users
		ON public.userDialogs USING btree
		(userId ASC NULLS LAST, dialogId ASC NULLS LAST)
		TABLESPACE pg_default;

		CREATE INDEX IF NOT EXISTS idx_user_dialogs
		ON public.userDialogs USING btree
		(dialogId ASC NULLS LAST)
		TABLESPACE pg_default;

		CREATE TABLE IF NOT EXISTS public.dialogs
		(
			id serial NOT NULL,
			CONSTRAINT pk_dialogs PRIMARY KEY (id)
		);

		CREATE TABLE IF NOT EXISTS public.messages
		(
			id serial NOT NULL,
			senderDialogId integer NOT NULL,
			text text COLLATE pg_catalog."C" NOT NULL,
			createdat time with time zone NOT NULL,
			read boolean NOT NULL default false,
			CONSTRAINT idx_messages PRIMARY KEY (id),
			CONSTRAINT "FK_message_user_dialogs" FOREIGN KEY (senderDialogId)
				REFERENCES public.userDialogs (id) MATCH SIMPLE
				ON UPDATE NO ACTION
				ON DELETE NO ACTION
				NOT VALID
		);

		CREATE INDEX IF NOT EXISTS idx_message_users
		ON public.messages USING btree
		(senderDialogId ASC NULLS LAST)
		TABLESPACE pg_default;

		CREATE TABLE IF NOT EXISTS public.events
		(
			id serial NOT NULL,
			dialogId integer NOT NULL,
			body text COLLATE pg_catalog."C" NOT NULL,
			sent boolean NOT NULL default false,
			received boolean NOT NULL default false,
			createdat time with time zone NOT NULL,
			expireat time with time zone,
			CONSTRAINT pk_events PRIMARY KEY (id)
		);`)

	if err != nil {
		fmt.Println(err)
		log.Fatal("init query failed")
	}
}
