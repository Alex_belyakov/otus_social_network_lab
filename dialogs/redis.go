package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strconv"
	"sync"

	"github.com/google/uuid"
	_ "github.com/lib/pq"
	"github.com/redis/go-redis/v9"
)

var (
	redisOnce       sync.Once
	rdb             *redis.Client
	sendScript      *redis.Script
	listScript      *redis.Script
	getDialogScript *redis.Script
	addDialogScript *redis.Script
)

func redisListDialog(ctx context.Context, userId int, friendId int, offset int, count int) (dialog Dialog, err error) {
	rdb := getRedisConnection()

	dialogId, err := getDialogScript.Run(ctx, rdb, nil, []interface{}{userId, friendId}).Int()
	dialog.Id = dialogId
	dialog.FriendId = friendId

	if err != nil || dialogId == 0 {
		return
	}

	if sset, err := listScript.Run(ctx, rdb, nil, []interface{}{dialogId, offset, count}).Result(); err == nil {
		messages := sset.([]interface{})
		for i := 0; i < len(messages); i += 2 {
			var message Message
			if msgJson, ok := messages[i].(string); ok {
				if err := json.Unmarshal([]byte(msgJson), &message); err != nil {
					return dialog, err
				}
			} else {
				return dialog, fmt.Errorf("cannot parse the message sset")
			}

			if idStr, ok := messages[i+1].(string); ok {
				if id, err := strconv.Atoi(idStr); err == nil {
					message.Id = id
					dialog.Messages = append(dialog.Messages, message)
				} else {
					return dialog, fmt.Errorf("cannot parse the message sset key")
				}
			} else {
				return dialog, fmt.Errorf("cannot parse the message sset key")
			}
		}
	}

	return
}

func redisSendMessage(ctx context.Context, userId int, friendId int, text string) (msgId int, err error) {
	rdb := getRedisConnection()

	dialogId, err := getDialogScript.Run(ctx, rdb, nil, []interface{}{userId, friendId}).Int()

	if err != nil {
		return
	}

	if dialogId == 0 {
		dialog, err := json.Marshal(
			struct {
				Uid      string `json:"uid"`
				UserId   int    `json:"userId"`
				FriendId int    `json:"friendId"`
			}{
				Uid:      uuid.New().String(),
				UserId:   userId,
				FriendId: friendId,
			})

		if err != nil {
			return 0, err
		}

		if dialogId, err = addDialogScript.Run(ctx, rdb, nil, []interface{}{userId, friendId, string(dialog)}).Int(); err != nil {
			return 0, err
		}
	}

	if msg, err := json.Marshal(
		struct {
			Uid      string `json:"uid"`
			DialogId int    `json:"dialogId"`
			Text     string `json:"text"`
		}{
			Uid:      uuid.New().String(),
			DialogId: dialogId,
			Text:     text,
		}); err == nil {
		return sendScript.Run(ctx, rdb, nil, []interface{}{dialogId, string(msg)}).Int()
	}

	return
}

func getRedisConnection() *redis.Client {
	redisOnce.Do(func() {
		redisConnection, exists := os.LookupEnv("redis_connection")

		if !exists {
			log.Fatal("redis connection env not found")
		}

		rdb = redis.NewClient(&redis.Options{
			Addr: redisConnection,
		})

		getDialogScript = redis.NewScript(`
		local userId = ARGV[1]
		local friendId = ARGV[2]
		local dialogIndexKey = string.format('%d:%d', userId, friendId)
		local dialogId = redis.call('SISMEMBER', 'dialogs:users', dialogIndexKey)
		
		return dialogId`)

		addDialogScript = redis.NewScript(`
		local userId = ARGV[1]
		local friendId = ARGV[2]
		local dialog = ARGV[3]
		local dialogIndexKey = string.format('%d:%d', userId, friendId)

		local dialogId = redis.call('ZRANGE', 'dialogs', 0, 0, 'REV', 'WITHSCORES')[2]
		if not dialogId then
			dialogId = 1
		else
			dialogId = dialogId + 1
		end

		redis.call('ZADD', 'dialogs', dialogId, dialog)
		redis.call('SADD', 'dialogs:users', dialogIndexKey, dialogId)
		
		return dialogId`)

		sendScript = redis.NewScript(`
		local dialogId = ARGV[1]
		local message = ARGV[2]

		local messageId = redis.call('ZRANGE', 'messages', 0, 0, 'REV', 'WITHSCORES')[2]
		if not messageId then
			messageId = 1
		else
			messageId = messageId + 1
		end

		local dialogMessagesSSet = string.format('dialog:%d:messages', dialogId)
		
		redis.call('ZADD', 'messages', messageId, message)
		redis.call('ZADD', dialogMessagesSSet, messageId, message)
		return messageId`)

		listScript = redis.NewScript(`
		local dialogId = ARGV[1]
		local offset = ARGV[2]
		local count = ARGV[3]

		local dialogMessagesSSet = string.format('dialog:%d:messages', dialogId)
		local first = offset
		local last = offset + count - 1
		local messages = redis.call('ZRANGE', dialogMessagesSSet, first, last, 'REV', 'WITHSCORES')
		
		return messages`)
	})

	return rdb
}
