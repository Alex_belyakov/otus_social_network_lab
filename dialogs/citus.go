package main

// import (
// 	"database/sql"
// 	"fmt"
// 	"log"
// 	"os"
// 	"sync"
// 	"time"

// 	_ "github.com/lib/pq"
// )

// var (
// 	citusOnce sync.Once
// 	citusConn *sql.DB
// )

// func getCitusConnection() *sql.DB {
// 	citusOnce.Do(func() {
// 		var err error

// 		connStr, exists := os.LookupEnv("citus_connection")

// 		if !exists {
// 			log.Fatal("citus_connection env not found")
// 		}

// 		if citusConn, err = sql.Open("postgres", connStr); err != nil {
// 			log.Fatal(err, "error getting the db connection")
// 		}
// 		citusConn.SetMaxOpenConns(5)
// 		citusConn.SetMaxIdleConns(5)
// 		citusConn.SetConnMaxLifetime(time.Minute)
// 	})
// 	return citusConn
// }

// func citusInit() {
// 	db := getCitusConnection()

// 	_, err := db.Exec(
// 		`CREATE TABLE IF NOT EXISTS public.dialogs
// 		(
// 			id serial NOT NULL,
// 			userid integer NOT NULL,
// 			friendid integer NOT NULL,
// 			CONSTRAINT pk_dialogs PRIMARY KEY (id, userid)
// 		);

// 		CREATE UNIQUE INDEX IF NOT EXISTS idx_friends
// 		ON public.dialogs USING btree
// 		(userid ASC NULLS LAST, friendid ASC NULLS LAST)
// 		TABLESPACE pg_default;

// 		CREATE TABLE IF NOT EXISTS public.messages
// 		(
// 			id serial NOT NULL,
// 			dialogid integer NOT NULL,
// 			userid integer NOT NULL,
// 			text text COLLATE pg_catalog."default" NOT NULL,
// 			createdat time with time zone NOT NULL,
// 			CONSTRAINT idx_messages PRIMARY KEY (id, userid)
// 		)`)

// 	if err != nil {
// 		fmt.Println(err)
// 		log.Fatal("init query failed")
// 	}
// }
