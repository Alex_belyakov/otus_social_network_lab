package main

import (
	"fmt"

	_ "github.com/lib/pq"
)

type Logger struct {
	ContextUid string
}

func (logger Logger) logInfo(message string) {
	fmt.Println(logger.ContextUid, message)
}

func (logger Logger) logError(message string, err error) {
	fmt.Println(logger.ContextUid, message, err)
}
