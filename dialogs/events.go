package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/IBM/sarama"
	_ "github.com/lib/pq"
)

const (
	sendEvent      = 1
	readEvent      = 2
	timeoutSeconds = 10
)

type Event struct {
	Id         int       `json:"id"`
	Type       int       `json:"type"`
	SenderId   int       `json:"senderId"`
	ReceiverId int       `json:"receiverId"`
	DialogId   int       `json:"dialogId"`
	Timestamp  time.Time `json:"timestamp"`
	MessageIds []int     `json:"messageIds"`
}

type ConfirmEvent struct {
	Id int `json:"id"`
}

type EventData struct {
	Id       int
	DialogId int
	Event    []byte
}

func startEventsResponseListener(ctx context.Context) (err error) {
	kafkaConnStr, exists := os.LookupEnv("kafka_connection")

	if !exists {
		log.Println("kafka connection env not found")
		return fmt.Errorf("kafka connection env not found")
	}

	group, exists := os.LookupEnv("event_replies_group")

	if !exists {
		log.Println("kafka group env not found")
		return fmt.Errorf("kafka group env not found")
	}

	kafkaPartitions, exists := os.LookupEnv("kafka_partitions")

	if !exists {
		log.Println("kafka partitions env not found")
		return fmt.Errorf("kafka partitions env not found")
	}

	partitionsCnt, err := strconv.Atoi(kafkaPartitions)

	if err != nil {
		log.Println("incorrect kafka partitions env")
		return fmt.Errorf("incorrect kafka partitions env")
	}

	brokerAddrs := []string{kafkaConnStr}
	config := sarama.NewConfig()
	config.Version = sarama.V2_1_0_0
	config.Consumer.Offsets.Initial = sarama.OffsetOldest
	config.Consumer.Group.Rebalance.GroupStrategies = []sarama.BalanceStrategy{sarama.NewBalanceStrategyRoundRobin()}

	admin, err := sarama.NewClusterAdmin(brokerAddrs, config)
	if err != nil {
		log.Fatal("Error while creating cluster admin: ", err.Error())
	}
	defer func() { _ = admin.Close() }()

	if topics, err := admin.ListTopics(); err == nil {
		if _, ok := topics["chat-event-replies"]; !ok {
			err = admin.CreateTopic("chat-event-replies", &sarama.TopicDetail{
				NumPartitions:     int32(partitionsCnt),
				ReplicationFactor: -1,
			}, false)
			if err != nil {
				log.Fatal("Error while creating topic: ", err.Error())
			}
		}
	} else {
		log.Fatal("Error while creating topic: ", err.Error())
	}

	client, err := sarama.NewConsumerGroup([]string{kafkaConnStr}, group, nil)
	if err != nil {
		log.Println("Failed to create consumer", err)
		return
	}

	consumer := Consumer{
		ready: make(chan bool),
	}

	go func() {
		for {
			// `Consume` should be called inside an infinite loop, when a
			// server-side rebalance happens, the consumer session will need to be
			// recreated to get the new claims
			if err := client.Consume(ctx, []string{"chat-event-replies"}, &consumer); err != nil {
				if errors.Is(err, sarama.ErrClosedConsumerGroup) {
					return
				}
				log.Panicf("Error from consumer: %v", err)
			}
			// check if context was cancelled, signaling that the consumer should stop
			if ctx.Err() != nil {
				return
			}
			consumer.ready = make(chan bool)
		}
	}()

	<-ctx.Done()
	log.Println("terminating: context cancelled")

	if err = client.Close(); err != nil {
		log.Panicf("Error closing client: %v", err)
	}

	return
}

func startEventsSender(ctx context.Context) (err error) {
	kafkaConnStr, exists := os.LookupEnv("kafka_connection")

	if !exists {
		log.Fatal("kafka connection env not found")
	}

	if !exists {
		log.Println("kafka connection env not found")
		return
	}

	go func() {
		// Создание продюсера Kafka
		producer, err := sarama.NewSyncProducer([]string{kafkaConnStr}, nil)
		if err != nil {
			log.Fatalf("Failed to create producer: %v", err)
		}

		defer producer.Close()

		for {
			select {
			case <-ctx.Done():
				return
			case <-time.Tick(1000 * time.Millisecond):
				events, err := dbGetUnansweredEvents(0, 10)

				if err != nil {
					log.Println("Error getting events", err)
					continue
				}

				if len(events) == 0 {
					if events, err = dbGetEvents(0, 10); err != nil {
						log.Println("Error getting events", err)
						continue
					}
				}

				for _, event := range events {
					msg := &sarama.ProducerMessage{
						Topic: "chat-events",
						Key:   sarama.StringEncoder(fmt.Sprint(event.DialogId)),
						Value: sarama.ByteEncoder(event.Event),
					}

					if _, _, err = producer.SendMessage(msg); err != nil {
						log.Println("Failed to send message to Kafka", err)
					}

					if err = dbMarkSentEvent(ctx, event.Id, timeoutSeconds); err != nil {
						log.Println("Failed to send message to Kafka", err)
					}
				}
			}
		}
	}()

	return
}

// Consumer represents a Sarama consumer group consumer
type Consumer struct {
	ready chan bool
}

// Setup is run at the beginning of a new session, before ConsumeClaim
func (consumer *Consumer) Setup(sarama.ConsumerGroupSession) error {
	// Mark the consumer as ready
	close(consumer.ready)
	return nil
}

// Cleanup is run at the end of a session, once all ConsumeClaim goroutines have exited
func (consumer *Consumer) Cleanup(sarama.ConsumerGroupSession) error {
	return nil
}

func (consumer *Consumer) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	for {
		select {
		case msg, ok := <-claim.Messages():
			if !ok {
				log.Println("Channel closed, exiting goroutine")
				return nil
			}

			var event ConfirmEvent
			err := json.Unmarshal(msg.Value, &event)

			if err != nil {
				log.Println("Error unmarshaling JSON:", err)
				continue
			}

			log.Printf("Received message: %+v\n", event)

			dbMarkReceivedEvent(event.Id)
			session.MarkMessage(msg, "")
		case <-session.Context().Done():
			return nil
		}
	}
}
