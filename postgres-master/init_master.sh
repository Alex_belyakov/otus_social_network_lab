sed -i 's/#ssl = off/ssl = off/g' /data/postgres/postgresql.conf 
sed -i 's/#wal_level = replica/wal_level = replica/g' /data/postgres/postgresql.conf 
sed -i 's/#max_wal_senders = 10/max_wal_senders = 4/g' /data/postgres/postgresql.conf 
echo "host    replication     replicator       172.30.0.0/16          md5" >> /data/postgres/pg_hba.conf 
psql -U postgres --command "create role replicator with login replication password 'pass'"

exit