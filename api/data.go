package main

import (
	"context"
	"crypto/aes"
	"crypto/cipher"
	"database/sql"
	"encoding/base64"
	"errors"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/google/uuid"
	_ "github.com/lib/pq"
)

type userModel struct {
	Name      string `json:"name"`
	Surname   string `json:"surname"`
	Age       byte   `json:"age"`
	Gender    byte   `json:"gender"`
	City      string `json:"city"`
	Interests string `json:"interests"`
}

type loginModel struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

type registerModel struct {
	Login    string    `json:"login"`
	Password string    `json:"password"`
	User     userModel `json:"user"`
}

type Post struct {
	UserId int    `json:"userId"`
	PostId int    `json:"postId"`
	Text   string `json:"text"`
}

func getFeedFromDb(userId int) (posts []Post, err error) {
	db := GetConnection()
	rows, err := db.Query(
		`select p.id, p.userid, p.text
		from users as u
		join friends as f on u.id = f.userid
		join posts as p on p.userid = f.friendid
		where u.id = $1 and deleted = 'false'
		order by id desc
		limit 1000`, userId)

	if err != nil {
		fmt.Println("query error", err)
		err = errors.New("query error")
		return
	}

	defer rows.Close()

	for rows.Next() {
		var p Post

		err = rows.Scan(&p.PostId, &p.UserId, &p.Text)

		if err != nil {
			fmt.Println("error reading posts by filter", err)
			return
		}

		posts = append(posts, p)
	}

	if posts == nil {
		posts = make([]Post, 0)
	}

	err = rows.Err()
	if err != nil {
		fmt.Println("error reading users by filter", err)
	}

	return
}

func searchUsersByFilter(namePrefix string, surnamePrefix string) (users []userModel, err error) {
	db := GetConnection()
	rows, err := db.Query("select name, surname, age, gender, city, interests from users where surname like $1 and name like $2 order by id", surnamePrefix+"%", namePrefix+"%")

	if err != nil {
		fmt.Println("query error", err)
		err = errors.New("query error")
		return
	}

	defer rows.Close()

	for rows.Next() {
		var u userModel
		var interests sql.NullString
		var city sql.NullString

		err = rows.Scan(&u.Name, &u.Surname, &u.Age, &u.Gender, &city, &interests)

		if err != nil {
			fmt.Println("error reading users by filter", err)
			return
		}

		u.Interests = interests.String
		u.City = city.String

		users = append(users, u)
	}

	err = rows.Err()
	if err != nil {
		fmt.Println("error reading users by filter", err)
	}

	return
}

func dbWriteUser(ctx context.Context, user registerModel) (err error) {
	db := GetConnection()
	tx, err := db.BeginTx(ctx, nil)

	if err != nil {
		fmt.Println("transaction begin failed", err)
		return
	}

	defer tx.Rollback()

	result := tx.QueryRowContext(ctx, "insert into users (name, surname, age, gender, city, interests) values ($1, $2, $3, $4, $5, $6) RETURNING id;",
		user.User.Name, user.User.Surname, user.User.Age, user.User.Gender, user.User.City, user.User.Interests)

	var userId int

	errIns := result.Scan(&userId)

	if errIns != nil {
		fmt.Println("get last insert id failed", err)
		return
	}

	salt, exists := os.LookupEnv("salt")

	if !exists {
		err = errors.New("social_network_db env not found")
		fmt.Println("salt env not found")
		return
	}

	safePassword, err := Encrypt(user.Password, salt)

	if err != nil {
		fmt.Println("error encrypting the password: ", err)
		return
	}

	safeToken, err := Encrypt(uuid.New().String(), salt)

	if err != nil {
		fmt.Println("error encrypting the token: ", err)
		return
	}

	result2, err := tx.ExecContext(ctx, "insert into logins (login, password, token, userId) values ($1, $2, $3, $4);",
		user.Login, safePassword, safeToken, userId)

	if err != nil {
		fmt.Println("insert logins failed", err)
		return
	}

	if err = tx.Commit(); err != nil {
		fmt.Println("commit failed", err)
		return
	}

	fmt.Println(result2.RowsAffected())

	return
}

func dbUserLoginExists(login string) (userExists bool, err error) {
	db := GetConnection()

	rows, err := db.Query("SELECT 1 FROM logins WHERE login=$1 limit 1", login)

	if err != nil {
		fmt.Println("get user by login error", err)
		return
	}

	defer rows.Close()

	for rows.Next() {
		fmt.Println("duplicate login")
		userExists = true
	}

	return
}

func dbReadUser(id string) (user *userModel, err error) {
	db := GetConnection()

	user = &userModel{}

	row := db.QueryRow("select name, surname, age, gender, city, interests from users where id = $1", id)

	var interests sql.NullString
	var city sql.NullString

	switch err = row.Scan(&user.Name, &user.Surname, &user.Age, &user.Gender, &city, &interests); err {
	case sql.ErrNoRows:
		log.Println("dialog not found", err)
	case nil:
		user.City = city.String
		user.Interests = interests.String
	default:
		log.Println("get dialog id query error", err)
	}

	return
}

func dbReadUserCookie(login string, password string) (token *string, err error) {
	var cookie string

	db := GetConnection()

	salt, exists := os.LookupEnv("salt")

	if !exists {
		log.Println("salt env not found")
		err = errors.New("salt env not found")
	}

	safePassword, err := Encrypt(password, salt)

	if err != nil {
		fmt.Println("error encrypting the password: ", err)
		return
	}

	rows, err := db.Query("select token from logins where login = $1 and password = $2", login, safePassword)

	if err != nil {
		log.Println("query error", err)
		return
	}

	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(&cookie)

		if err != nil {
			return
		}

		decrypted, errDecrypt := Decrypt(cookie, salt)

		if err != nil {
			fmt.Println("error decrypting the token: ", errDecrypt)
			return token, errDecrypt
		}

		token = &decrypted
	}
	err = rows.Err()
	if err != nil {
		return
	}

	return
}

func dbCheckToken(token string) (err error) {

	db := GetConnection()

	salt, exists := os.LookupEnv("salt")

	if !exists {
		fmt.Println("salt env not found")
		err = errors.New("salt env not found")
		return
	}

	encrypted, err := Encrypt(token, salt)

	if err != nil {
		fmt.Println("error encrypting the token: ", err)
		return
	}

	rows, err := db.Query("select 1 from logins where token = $1 limit 1", encrypted)

	if err != nil {
		log.Println("check token query error", err)
		return
	}

	defer rows.Close()

	for rows.Next() {
		return
	}

	log.Println("token not found")
	return errors.New("token not found")
}

func dbgetUserIdByToken(token string) (id int, err error) {

	db := GetConnection()

	salt, exists := os.LookupEnv("salt")

	if !exists {
		fmt.Println("salt env not found")
		err = errors.New("salt env not found")
		return
	}

	encrypted, err := Encrypt(token, salt)

	if err != nil {
		fmt.Println("error encrypting the token: ", err)
		return
	}

	row := db.QueryRow("select userId from logins where token = $1 limit 1", encrypted)
	switch err := row.Scan(&id); err {
	case sql.ErrNoRows:
		log.Println("token not found", err)
	case nil:
	default:
		log.Println("get user id by token query error", err)
	}

	return
}

func dbCreatePost(ctx context.Context, userId int, text string) (postId int, err error) {
	db := GetConnection()
	tx, err := db.BeginTx(ctx, nil)

	if err != nil {
		fmt.Println("transaction begin failed", err)
		return
	}

	defer tx.Rollback()

	now := time.Now()

	result := tx.QueryRowContext(ctx,
		`insert into public.posts(
		userid, createdat, updatedat, text, deleted)
		values ($1, $2, $3, $4, 'false') returning id;`,
		userId, now, now, text)

	err = result.Scan(&postId)

	if err != nil {
		fmt.Println("crete post failed", err)
		return
	}

	if err = tx.Commit(); err != nil {
		fmt.Println("commit failed", err)
		return
	}

	return
}

func dbMakeFriends(ctx context.Context, userId int, friendId int) (err error) {
	db := GetConnection()
	tx, err := db.BeginTx(ctx, nil)

	if err != nil {
		fmt.Println("transaction begin failed", err)
		return
	}

	defer tx.Rollback()

	_, err = tx.ExecContext(ctx,
		`insert into public.friends(
		userid, friendId)
		values ($1, $2);`,
		userId, friendId)

	if err != nil {
		fmt.Println("make friends failed", err)
		return
	}

	if err = tx.Commit(); err != nil {
		fmt.Println("commit failed", err)
		return
	}

	return
}

func dbRemoveFriend(ctx context.Context, userId int, friendId int) (err error) {
	db := GetConnection()
	tx, err := db.BeginTx(ctx, nil)

	if err != nil {
		fmt.Println("transaction begin failed", err)
		return
	}

	defer tx.Rollback()

	_, err = tx.ExecContext(ctx,
		`delete from friends
		where userid = $1 and friendid = $2;`,
		userId, friendId)

	if err != nil {
		fmt.Println("delete friends failed", err)
		return
	}

	if err = tx.Commit(); err != nil {
		fmt.Println("commit failed", err)
		return
	}

	return
}

func dbGetFriends(userId int, offset int, count int) (ids []int, err error) {
	db := GetConnection()
	rows, err := db.Query("select friendid from friends where userid = $1 offset $2 limit $3", userId, offset, count)

	if err != nil {
		fmt.Println("query error", err)
		err = errors.New("query error")
		return
	}

	defer rows.Close()

	for rows.Next() {
		var id int

		err = rows.Scan(&id)

		if err != nil {
			fmt.Println("error reading friends", err)
			return
		}

		ids = append(ids, id)
	}

	err = rows.Err()
	if err != nil {
		fmt.Println("error reading users by filter", err)
	}

	return
}

func dbGetUserPosts(userId int) (posts []Post, err error) {
	db := GetConnection()
	rows, err := db.Query("select id, userid, text from posts where userid = $1 order by id desc limit 1000", userId)

	if err != nil {
		fmt.Println("query error", err)
		err = errors.New("query error")
		return
	}

	defer rows.Close()

	for rows.Next() {
		var post Post

		err = rows.Scan(&post.PostId, &post.UserId, &post.Text)

		if err != nil {
			fmt.Println("error reading friends", err)
			return
		}

		posts = append(posts, post)
	}

	err = rows.Err()
	if err != nil {
		fmt.Println("error reading user posts", err)
	}

	return
}

func dbUpdatePost(ctx context.Context, postId int, userId int, text string) (newPostId int, err error) {
	db := GetConnection()
	tx, err := db.BeginTx(ctx, nil)

	if err != nil {
		fmt.Println("transaction begin failed", err)
		return
	}

	defer tx.Rollback()

	now := time.Now()

	updResult, err := tx.ExecContext(ctx,
		`update public.posts
		set deleted = 'true', updatedat = $1
		where id = $2 and userid = $3`,
		now, postId, userId)

	if err != nil {
		fmt.Println("create post failed", err)
		return
	}

	if updated, _ := updResult.RowsAffected(); updated > 0 {
		result := tx.QueryRowContext(ctx,
			`insert into public.posts(
			userid, createdat, updatedat, text, deleted)
			values ($1, $2, $3, $4, 'false')
			returning id;`,
			userId, now, now, text)

		err = result.Scan(&newPostId)

		if err != nil {
			fmt.Println("update post failed", err)
			return
		}

		if err = tx.Commit(); err != nil {
			fmt.Println("commit failed", err)
			return
		}
	}

	return
}

func dbDeletePost(ctx context.Context, postId int, userId int) (err error) {
	db := GetConnection()
	tx, err := db.BeginTx(ctx, nil)

	if err != nil {
		fmt.Println("transaction begin failed", err)
		return
	}

	defer tx.Rollback()

	now := time.Now()

	_, err = tx.ExecContext(ctx,
		`update posts
		set deleted = 'true', updatedat = $1
		where userid = $2 and id = $3`,
		now, userId, postId)

	if err != nil {
		fmt.Println("create post failed", err)
		return
	}

	if err = tx.Commit(); err != nil {
		fmt.Println("commit failed", err)
		return
	}

	return
}

func dbgetPost(postId int) (post Post, err error) {

	db := GetConnection()

	row := db.QueryRow("select id, userId, text from posts where id = $1 limit 1", postId)
	switch err := row.Scan(&post.PostId, &post.UserId, &post.Text); err {
	case sql.ErrNoRows:
		log.Println("post not found", err)
	case nil:
	default:
		log.Println("get user id by token query error", err)
	}

	return
}

func GetConnection() *sql.DB {
	once.Do(func() {
		var err error

		connStr, exists := os.LookupEnv("social_network_db")

		if !exists {
			log.Fatal("social_network_db env not found")
		}

		if conn, err = sql.Open("postgres", connStr); err != nil {
			log.Fatal(err, "error getting the db connection")
		}
		conn.SetMaxOpenConns(5)
		conn.SetMaxIdleConns(5)
		conn.SetConnMaxLifetime(time.Minute)
	})
	return conn
}

func dbInit() {
	postgresStr, exists := os.LookupEnv("postgres_db")

	if !exists {
		log.Fatal("postgres_db env not found")
	}

	postgresDb, err := sql.Open("postgres", postgresStr)

	if err != nil {
		fmt.Println(err)
		log.Fatal("init connection failed")
	}

	defer postgresDb.Close()

	rows, err := postgresDb.Query("SELECT 1 FROM pg_catalog.pg_database WHERE datname='otus_social_network' limit 1")

	if err != nil {
		fmt.Println(err)
		log.Fatal("get db error")
	}

	defer rows.Close()

	for rows.Next() {
		return
	}

	result1, err := postgresDb.Exec(`CREATE DATABASE otus_social_network`)

	if err != nil {
		fmt.Println(err)
		log.Fatal("create db failed")
	}

	db := GetConnection()

	result2, err := db.Exec(
		`CREATE TABLE IF NOT EXISTS public.users
		(
			id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
			name character varying COLLATE pg_catalog."C" NOT NULL,
			surname character varying COLLATE pg_catalog."C" NOT NULL,
			age smallint NOT NULL,
			gender smallint NOT NULL,
			city character varying COLLATE pg_catalog."C",
			interests character varying COLLATE pg_catalog."C",
			CONSTRAINT users_pkey PRIMARY KEY (id)
		)
		
		TABLESPACE pg_default;
		
		ALTER TABLE IF EXISTS public.users
			OWNER to postgres;
			
		CREATE TABLE IF NOT EXISTS public.logins
		(
			id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
			login character varying COLLATE pg_catalog."C" NOT NULL,
			token character varying COLLATE pg_catalog."C" NOT NULL,
			password character varying COLLATE pg_catalog."C" NOT NULL,
			userid integer NOT NULL,
			CONSTRAINT logins_pkey PRIMARY KEY (id),
			CONSTRAINT "FK_user_login" FOREIGN KEY (userid)
				REFERENCES public.users (id) MATCH SIMPLE
				ON UPDATE NO ACTION
				ON DELETE NO ACTION
				NOT VALID
		)
		
		TABLESPACE pg_default;
		
		ALTER TABLE IF EXISTS public.logins
			OWNER to postgres;		
		
		CREATE INDEX IF NOT EXISTS idx_name_surname
		ON public.users USING btree
		(name COLLATE pg_catalog."C" ASC NULLS LAST, surname COLLATE pg_catalog."C" ASC NULLS LAST)
		TABLESPACE pg_default;
		
		CREATE TABLE IF NOT EXISTS public.friends
		(
			userid integer NOT NULL,
			friendid integer NOT NULL,
			CONSTRAINT friends_pkey PRIMARY KEY (userid, friendid),
			CONSTRAINT "idx_friends_friendId" FOREIGN KEY (friendid)
				REFERENCES public.users (id) MATCH SIMPLE
				ON UPDATE NO ACTION
				ON DELETE NO ACTION
				NOT VALID,
			CONSTRAINT idx_friends_user_id FOREIGN KEY (userid)
				REFERENCES public.users (id) MATCH SIMPLE
				ON UPDATE NO ACTION
				ON DELETE NO ACTION
				NOT VALID
		)

		TABLESPACE pg_default;

		ALTER TABLE IF EXISTS public.friends
			OWNER to postgres;	
						
		CREATE TABLE IF NOT EXISTS public.posts
		(
			id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
			userid integer NOT NULL,
			createdat time with time zone NOT NULL,
			updatedat time with time zone NOT NULL,
			text text COLLATE pg_catalog."C" NOT NULL,
			deleted boolean NOT NULL DEFAULT false,
			CONSTRAINT posts_pkey PRIMARY KEY (id),
			CONSTRAINT ids_posts_user_id FOREIGN KEY (userid)
				REFERENCES public.users (id) MATCH SIMPLE
				ON UPDATE NO ACTION
				ON DELETE NO ACTION
				NOT VALID
		)

		TABLESPACE pg_default;

		ALTER TABLE IF EXISTS public.posts
			OWNER to postgres;	`)

	if err != nil {
		fmt.Println(err)
		log.Fatal("init query failed")
	}

	fmt.Println(result1.RowsAffected())
	fmt.Println(result2.RowsAffected())
}

var encryptBytes = []byte{35, 46, 57, 24, 85, 35, 24, 74, 87, 35, 88, 98, 66, 32, 14, 05}

func Encode(b []byte) string {
	return base64.StdEncoding.EncodeToString(b)
}

func Encrypt(text, MySecret string) (string, error) {
	block, err := aes.NewCipher([]byte(MySecret))
	if err != nil {
		return "", err
	}
	plainText := []byte(text)
	cfb := cipher.NewCFBEncrypter(block, encryptBytes)
	cipherText := make([]byte, len(plainText))
	cfb.XORKeyStream(cipherText, plainText)
	return Encode(cipherText), nil
}

func Decode(s string) ([]byte, error) {
	data, err := base64.StdEncoding.DecodeString(s)
	if err != nil {
		return nil, err
	}
	return data, nil
}

// Decrypt method is to extract back the encrypted text
func Decrypt(text, MySecret string) (string, error) {
	block, err := aes.NewCipher([]byte(MySecret))
	if err != nil {
		return "", err
	}
	cipherText, err := Decode(text)
	if err != nil {
		return "", err
	}
	cfb := cipher.NewCFBDecrypter(block, encryptBytes)
	plainText := make([]byte, len(cipherText))
	cfb.XORKeyStream(plainText, cipherText)
	return string(plainText), nil
}
