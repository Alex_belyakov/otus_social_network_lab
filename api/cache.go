package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/go-redis/cache/v9"
	"github.com/google/uuid"
	_ "github.com/lib/pq"
	"github.com/redis/go-redis/v9"
)

func removeFeedCache(ctx context.Context, userId int) error {
	redisConnection, exists := os.LookupEnv("redis_connection")

	if !exists {
		log.Fatal("redis connection env not found")
	}
	rdb := redis.NewClient(&redis.Options{
		Addr: redisConnection,
	})

	mycache := cache.New(&cache.Options{
		Redis: rdb,
	})

	err := mycache.Delete(ctx, getFeedStr(userId))

	switch err {
	case cache.ErrCacheMiss:
		return nil
	case nil:
		return nil
	default:
		{
			log.Println("error removing feed cache", err)
			return err
		}
	}
}

func createPostCache(ctx context.Context, postId int, post Post) (err error) {
	redisConnection, exists := os.LookupEnv("redis_connection")

	if !exists {
		log.Fatal("redis connection env not found")
	}
	rdb := redis.NewClient(&redis.Options{
		Addr: redisConnection,
	})

	mycache := cache.New(&cache.Options{
		Redis: rdb,
	})

	if err := mycache.Set(&cache.Item{
		Ctx:   ctx,
		Key:   getPostStr(postId),
		Value: post,
		TTL:   time.Hour,
	}); err != nil {
		log.Println("error caching the post", err)
	}

	return
}

func getPostCache(ctx context.Context, postId int) (post *Post, err error) {
	redisConnection, exists := os.LookupEnv("redis_connection")

	if !exists {
		log.Fatal("redis connection env not found")
	}
	rdb := redis.NewClient(&redis.Options{
		Addr: redisConnection,
	})

	mycache := cache.New(&cache.Options{
		Redis: rdb,
	})

	key := getPostStr(postId)

	err = mycache.Get(ctx, key, &post)

	switch err {
	case cache.ErrCacheMiss:
		return nil, nil
	case nil:
		fmt.Println("get the feed from cache for the post", post.PostId)
		_, expErr := rdb.Expire(ctx, key, time.Hour.Abs()).Result()
		if expErr != nil {
			fmt.Println("set expiration error", expErr)
			return nil, expErr
		}
		return post, nil
	default:
		{
			log.Println("error getting feed", err)
			return nil, err
		}
	}
}

func createFeedCache(ctx context.Context, userId int, feed []Post) error {
	return acquireFeedLockAndRun(ctx, userId, func(ctx context.Context, rdb *redis.Client, userId int) (err error) {
		mycache := cache.New(&cache.Options{
			Redis: rdb,
		})
		if err := mycache.Set(&cache.Item{
			Ctx:   ctx,
			Key:   getFeedStr(userId),
			Value: feed,
			TTL:   time.Hour,
		}); err != nil {
			log.Println("error locking the feed", err)
		}

		return
	})
}

func updateFeedCache(ctx context.Context, userId int, orderedPosts []Post) error {
	return acquireFeedLockAndRun(ctx, userId, func(ctx context.Context, rdb *redis.Client, userId int) (err error) {
		mycache := cache.New(&cache.Options{
			Redis: rdb,
		})

		var feed []Post
		updatedFeed := make([]Post, 0)
		feedKey := getFeedStr(userId)

		err = mycache.Get(ctx, feedKey, &feed)

		if err == cache.ErrCacheMiss {
			log.Println("feed cache miss")
			return nil
		}

		if err != nil {
			log.Println("error locking the feed", err)
			return err
		}

		if len(feed) == 0 {
			if err := mycache.Set(&cache.Item{
				Ctx:   ctx,
				Key:   getFeedStr(userId),
				Value: orderedPosts,
				TTL:   time.Hour,
			}); err != nil {
				log.Println("error creating the feed", err)
			}
			return err
		}

		oldIndex := 0
		newIndex := 0

		for len(updatedFeed) < 1000 {
			if newIndex == len(orderedPosts) {
				if oldIndex == len(feed) {
					break
				}
				// todo: replace with copy the remaining slice
				updatedFeed = append(updatedFeed, feed[oldIndex])
				oldIndex++
				continue
			}

			if oldIndex == len(feed) {
				// todo: replace with copy the remaining slice
				updatedFeed = append(updatedFeed, orderedPosts[newIndex])
				newIndex++
				continue
			}

			if feed[oldIndex].PostId == orderedPosts[newIndex].PostId {
				updatedFeed = append(updatedFeed, feed[oldIndex])
				oldIndex++
				newIndex++
				continue
			}

			if feed[oldIndex].PostId > orderedPosts[newIndex].PostId {
				updatedFeed = append(updatedFeed, feed[oldIndex])
				oldIndex++
			} else {
				updatedFeed = append(updatedFeed, orderedPosts[newIndex])
				newIndex++
			}
		}

		if err := mycache.Set(&cache.Item{
			Ctx:   ctx,
			Key:   getFeedStr(userId),
			Value: updatedFeed,
			TTL:   time.Hour,
		}); err != nil {
			log.Println("error updating the feed", err)
		}
		return err
	})
}

func removeFriendFeedCache(ctx context.Context, userId int, friendId int) error {
	return acquireFeedLockAndRun(ctx, userId, func(ctx context.Context, rdb *redis.Client, userId int) (err error) {
		mycache := cache.New(&cache.Options{
			Redis: rdb,
		})

		var feed []Post
		updatedFeed := make([]Post, 0)
		feedKey := getFeedStr(userId)

		err = mycache.Get(ctx, feedKey, &feed)
		if err != nil {
			log.Println("error locking the feed", err)
			return
		}

		for _, post := range feed {
			if post.UserId != friendId {
				updatedFeed = append(updatedFeed, post)
			}
		}

		if err := mycache.Set(&cache.Item{
			Ctx:   ctx,
			Key:   feedKey,
			Value: updatedFeed,
			TTL:   time.Hour,
		}); err != nil {
			log.Println("error caching the post", err)
		} else {
			log.Println("error updating the feed", err)
			return err
		}

		return
	})
}

func removePostFeedCache(ctx context.Context, userId int, postId int, publisherId int) error {
	return acquireFeedLockAndRun(ctx, userId, func(ctx context.Context, rdb *redis.Client, userId int) (err error) {
		mycache := cache.New(&cache.Options{
			Redis: rdb,
		})

		var feed []Post
		updatedFeed := make([]Post, 0)
		feedKey := getFeedStr(userId)

		err = mycache.Get(ctx, feedKey, &feed)

		if err == cache.ErrCacheMiss {
			return nil
		}

		if err != nil {
			log.Println("feed cache is empty", err)
			return err
		}

		for _, post := range feed {
			if post.PostId != postId || post.UserId != publisherId {
				updatedFeed = append(updatedFeed, post)
			}
		}

		if err := mycache.Set(&cache.Item{
			Ctx:   ctx,
			Key:   feedKey,
			Value: updatedFeed,
			TTL:   time.Hour,
		}); err != nil {
			log.Println("error updating the feed", err)
		}

		return err
	})
}

func getFeedCache(ctx context.Context, userId int) ([]Post, error) {
	redisConnection, exists := os.LookupEnv("redis_connection")

	if !exists {
		log.Fatal("redis connection env not found")
	}
	rdb := redis.NewClient(&redis.Options{
		Addr: redisConnection,
	})

	mycache := cache.New(&cache.Options{
		Redis: rdb,
	})

	var feed []Post

	key := getFeedStr(userId)
	err := mycache.Get(ctx, key, &feed)

	switch err {
	case cache.ErrCacheMiss:
		return nil, nil
	case nil:
		fmt.Println("get the feed from cache for user", userId)
		_, expErr := rdb.Expire(ctx, key, time.Hour.Abs()).Result()
		if expErr != nil {
			fmt.Println("set expiration error", expErr)
			return nil, expErr
		}
		return feed, nil
	default:
		{
			log.Println("error getting feed", err)
			return nil, err
		}
	}
}

func acquireFeedLockAndRun(ctx context.Context, userId int, f func(ctx context.Context, rdb *redis.Client, userId int) error) error {

	redisConnection, exists := os.LookupEnv("redis_connection")

	if !exists {
		log.Fatal("redis connection env not found")
	}
	rdb := redis.NewClient(&redis.Options{
		Addr: redisConnection,
	})

	defer releaseFeedLock(ctx, *rdb, userId)

	const maxRetries = 70
	lockKey := getFeedLockStr(userId)
	var lockUid string

	tx := func(tx *redis.Tx) error {
		uid, err := rdb.Get(ctx, lockKey).Result()
		if err != nil || lockUid != uid {
			return fmt.Errorf("lock conflict")
		}

		_, err = tx.TxPipelined(ctx, func(pipe redis.Pipeliner) error {
			return f(ctx, rdb, userId)
		})

		return err
	}

	for i := 0; i < maxRetries; i++ {
		_, err := rdb.Get(ctx, lockKey).Result()

		switch err {
		case redis.Nil:
			lockUid = uuid.New().String()
			err := rdb.Set(ctx, lockKey, lockUid, time.Minute.Abs()).Err()
			if err != nil {
				log.Println("error locking the feed", err)
			}
			if wErr := rdb.Watch(ctx, tx, lockKey); wErr == nil {
				return nil
			}
		case nil:
			time.Sleep(time.Second)
		default:
			log.Println("error locking the feed", err)
			time.Sleep(time.Second)
		}
	}

	return fmt.Errorf("feed failed to update")
}

func releaseFeedLock(ctx context.Context, rdb redis.Client, userId int) (err error) {
	err = rdb.Del(ctx, getFeedLockStr(userId)).Err()
	if err != nil {
		log.Println(err)
	}
	return
}

func getFeedLockStr(userId int) string {
	return fmt.Sprintf("feed-user-%d-lock", userId)
}

func getFeedStr(userId int) string {
	return fmt.Sprintf("feed-user-%d", userId)
}

func getPostStr(postId int) string {
	return fmt.Sprintf("post-%d", postId)
}
