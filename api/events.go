package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"sync"

	"github.com/IBM/sarama"
	"github.com/google/uuid"
	_ "github.com/lib/pq"
)

var usersMu sync.RWMutex
var publisherIds map[int]bool

const (
	addPosts  = 0
	rmPost    = 1
	rmFriend  = 2
	newFriend = 3
)

type Event struct {
	Type        int    `json:"type"`
	Posts       []Post `json:"posts"`
	PublisherId int    `json:"postPublisherId"`
	ConsumerId  int    `json:"postConsumerId"`
}

type PostChannelEvent struct {
	Post      Post  `json:"post"`
	FriendIds []int `json:"friendIds"`
}

func startUserEvents(ctx context.Context, userId int) (err error) {
	usersMu.RLock()
	if publisherIds[userId] {
		usersMu.RUnlock()
		return
	}

	usersMu.RUnlock()
	usersMu.Lock()
	defer usersMu.Unlock()

	if publisherIds[userId] {
		return
	}

	kafkaConnStr, exists := os.LookupEnv("kafka_connection")

	if !exists {
		log.Println("kafka connection env not found")
		return
	}
	// todo: AutoCommit.Enable = false
	//config := &sarama.Config{}
	//config.Consumer.Offsets.AutoCommit.Enable = false
	consumer, err := sarama.NewConsumer([]string{kafkaConnStr}, nil)
	if err != nil {
		log.Println("Failed to create consumer", err)
		return
	}

	partConsumer, err := consumer.ConsumePartition(fmt.Sprintf("events-user-%d", userId), 0, sarama.OffsetNewest)
	if err != nil {
		log.Println("Failed to consume partition", err)
		return
	}

	go func() {
		defer partConsumer.Close()
		defer consumer.Close()

		for {
			select {
			case <-ctx.Done():
				return
			case msg, ok := <-partConsumer.Messages():
				if !ok {
					log.Println("Channel closed, exiting goroutine")
					return
				}

				var event Event
				err := json.Unmarshal(msg.Value, &event)

				if err != nil {
					log.Printf("Error unmarshaling JSON: %v\n", err)
					continue
				}

				log.Printf("Received message: %+v\n", event)

				switch event.Type {
				case addPosts:
					err = handlePostAdded(ctx, event)
				case rmPost:
					err = handleRemovePost(ctx, event)
				case rmFriend:
					err = handleRemoveFriend(ctx, event)
				case newFriend:
					err = handleFriendAdded(ctx, event)
				default:
					log.Println("Error: event type not supported")
				}

				if err != nil {
					publishError(ctx, event)
				}
			}
		}
	}()

	if publisherIds == nil {
		publisherIds = make(map[int]bool)
	}
	publisherIds[userId] = true

	return
}

func publish(ctx context.Context, event Event) error {
	return publishTopic(ctx, event, fmt.Sprintf("events-user-%d", event.PublisherId))
}

func publishError(ctx context.Context, event Event) error {
	return publishTopic(ctx, event, "events-user-errors")
}

func publishTopic(ctx context.Context, event Event, topic string) (err error) {
	if err = startUserEvents(ctx, event.PublisherId); err == nil {
		err = publishMsgTopic(event, topic)
	}

	return
}

func publishMsgTopic(eventMsg interface{}, topic string) (err error) {
	kafkaConnStr, exists := os.LookupEnv("kafka_connection")

	if !exists {
		log.Fatal("kafka connection env not found")
	}

	// Создание продюсера Kafka
	producer, err := sarama.NewSyncProducer([]string{kafkaConnStr}, nil)
	if err != nil {
		log.Fatalf("Failed to create producer: %v", err)
	}

	defer producer.Close()

	requestID := uuid.New().String()

	// Преобразование сообщения в JSON что бы потом отправить через kafka
	bytes, err := json.Marshal(eventMsg)

	msg := &sarama.ProducerMessage{
		Topic: topic,
		Key:   sarama.StringEncoder(requestID),
		Value: sarama.ByteEncoder(bytes),
	}

	// отправка сообщения в Kafka
	_, _, err = producer.SendMessage(msg)
	if err != nil {
		log.Println("Failed to send message to Kafka", err)
	}

	return
}

// todo: for popular persons update in parallel
func handlePostAdded(ctx context.Context, event Event) (err error) {
	offset := 0
	step := 1000
	friendIds, err := dbGetFriends(event.PublisherId, offset, step)

	for friendIds != nil && err == nil {
		for _, id := range friendIds {
			if err = updateFeedCache(ctx, id, event.Posts); err != nil {
				return
			}
		}
		offset += step
		if err = publishMsgTopic(PostChannelEvent{Post: event.Posts[0], FriendIds: friendIds}, "post-feed"); err != nil {
			return
		}
		friendIds, err = dbGetFriends(event.PublisherId, offset, step)
	}

	return
}

// todo: for popular persons update in parallel
func handleFriendAdded(ctx context.Context, event Event) (err error) {
	offset := 0
	step := 1000
	friendIds, err := dbGetFriends(event.PublisherId, offset, step)

	for friendIds != nil && err == nil {
		for _, id := range friendIds {
			if err = updateFeedCache(ctx, id, event.Posts); err != nil {
				return
			}
		}
		offset += step
		friendIds, err = dbGetFriends(event.PublisherId, offset, step)
	}

	return
}

func handleRemoveFriend(ctx context.Context, event Event) error {
	return removeFriendFeedCache(ctx, event.ConsumerId, event.PublisherId)
}

func handleRemovePost(ctx context.Context, event Event) (err error) {
	offset := 0
	step := 1000
	friendIds, err := dbGetFriends(event.PublisherId, offset, step)

	for friendIds != nil && err == nil {
		for _, id := range friendIds {
			if err = removePostFeedCache(ctx, id, event.Posts[0].PostId, event.PublisherId); err != nil {
				return
			}
		}
		offset += step
		friendIds, err = dbGetFriends(event.PublisherId, offset, step)
	}

	return
}
