package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/google/uuid"
)

type MessageModel struct {
	Text string `json:"text"`
}

type Dialog struct {
	Id       int       `json:"id"`
	FriendId int       `json:"friendId"`
	Messages []Message `json:"messages"`
}

type Message struct {
	Id   int    `json:"id"`
	Text string `json:"text"`
}

func apiListDialog(userId int, friendId int, offset int, count int) (dialog Dialog, err error) {
	baseurl, exists := os.LookupEnv("dialogs_url")
	if !exists {
		log.Println("api base url env not found")
		return
	}

	req, err := http.NewRequest("GET", fmt.Sprintf("%s/dialog/from/%d/to/%d?offset=%d&count=%d", baseurl, userId, friendId, offset, count), nil)
	if err != nil {
		log.Println("api error", err)
		return
	}

	secretKey, exists := os.LookupEnv("secret_key")

	if !exists {
		err = fmt.Errorf("secret_key env not found")
		return
	}

	requestUid := uuid.New().String()
	logger := Logger{ContextUid: requestUid}

	req.Header.Set("Accept", "application/json")
	req.Header.Set("Secret-Key", secretKey)
	req.Header.Set("Request-Uid", requestUid)

	client := &http.Client{}

	logger.logInfo(fmt.Sprintf("listing the dialog userId: %d friendId: %d", userId, friendId))
	resp, err := client.Do(req)

	if err != nil {
		logger.logError("error making http request: %s\n", err)
		return
	}

	logger.logInfo(fmt.Sprintf("client: status code: %d\n", resp.StatusCode))

	if resp.StatusCode == 200 {
		defer resp.Body.Close()

		b, err := io.ReadAll(resp.Body)

		if err == nil {
			if err = json.Unmarshal(b, &dialog); err != nil {
				logger.logError("error parsing the dialog response", err)
			}
		}
	} else {
		logger.logError("api send message failed", err)
		err = fmt.Errorf("api send message failed")
	}

	return
}

func apiSendMessage(userId int, friendId int, text string) (id int, err error) {
	baseurl, exists := os.LookupEnv("dialogs_url")
	if !exists {
		log.Println("api base url env not found")
		return
	}

	jsonBody := []byte(fmt.Sprintf(`{"text": "%s"}`, text))
	bodyReader := bytes.NewReader(jsonBody)
	req, err := http.NewRequest("POST", fmt.Sprintf("%s/dialog/from/%d/to/%d", baseurl, userId, friendId), bodyReader)
	if err != nil {
		log.Println("api error", err)
		return
	}

	secretKey, exists := os.LookupEnv("secret_key")

	if !exists {
		err = fmt.Errorf("secret_key env not found")
		return
	}

	requestUid := uuid.New().String()
	logger := Logger{ContextUid: requestUid}

	req.Header.Set("Accept", "application/json")
	req.Header.Set("Secret-Key", secretKey)
	req.Header.Set("Request-Uid", requestUid)

	client := &http.Client{}

	logger.logInfo(fmt.Sprintf("sending the message userId: %d friendId: %d", userId, friendId))
	resp, err := client.Do(req)

	if err != nil {
		logger.logError("error making http request: %s\n", err)
		return
	}

	logger.logInfo(fmt.Sprintf("client: status code: %d\n", resp.StatusCode))

	if resp.StatusCode == 200 {
		defer resp.Body.Close()

		b, err := io.ReadAll(resp.Body)

		if err == nil {
			var msg Message
			if err = json.Unmarshal(b, &msg); err == nil {
				id = msg.Id
			}
		} else {
			logger.logError("error parsing the sending message response", err)
		}
	} else {
		logger.logError("api send message failed", err)
		err = fmt.Errorf("api send message failed")
	}

	return
}
