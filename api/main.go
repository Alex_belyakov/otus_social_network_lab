package main

import (
	"context"
	"database/sql"
	"log"
	"math"
	"net/http"
	"strconv"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	httpProcessedRequests = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "api_http_requests_total",
		Help: "The total number of api http requests",
	}, []string{"path"})
	httpDuration = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name: "api_http_duration_seconds",
		Help: "Duration of HTTP requests.",
	}, []string{"path"})
	once    sync.Once
	conn    *sql.DB
	exitCtx context.Context
)

type PostModel struct {
	PostId int    `json:"postId"`
	Text   string `json:"text"`
}

func prometheusMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		path := c.Request.URL.Path
		timer := prometheus.NewTimer(httpDuration.WithLabelValues(path))
		c.Next()

		httpProcessedRequests.WithLabelValues(path).Inc()

		timer.ObserveDuration()
	}
}

func login(c *gin.Context) {
	var login loginModel

	if err := c.BindJSON(&login); err != nil {
		return
	}

	token, err := dbReadUserCookie(login.Login, login.Password)

	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "login failure, please try again"})
		return
	}

	if token == nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "invalid credentials"})
		return
	}

	c.SetCookie("token", *token, int(math.Ceil(time.Hour.Seconds())), "/", "localhost", false, true)
}

func register(c *gin.Context) {
	var newUser registerModel

	if err := c.BindJSON(&newUser); err != nil {
		log.Print("model error")
		return
	}

	userExists, err := dbUserLoginExists(newUser.Login)
	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "registration failure, please try again"})
		return
	}

	if userExists {
		c.IndentedJSON(http.StatusConflict, gin.H{"message": "login already occupied"})
		return
	}

	if err := dbWriteUser(c.Request.Context(), newUser); err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "registration failure, please try again"})
		return
	}

	c.IndentedJSON(http.StatusCreated, newUser.User)
}

func getUsersByFilter(c *gin.Context) {
	token, err := c.Cookie("token")

	if err != nil {
		log.Println("token not set")
		return
	}

	if err = dbCheckToken(token); err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	namePrefix := c.DefaultQuery("nameprefix", "")
	surnamePrefix := c.DefaultQuery("surnameprefix", "")

	if namePrefix == "" || surnamePrefix == "" {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"message": "please enter filtering strings"})
		return
	}

	users, err := searchUsersByFilter(namePrefix, surnamePrefix)

	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "user get failure, please try again"})
		return
	}

	c.IndentedJSON(http.StatusOK, users)
}

func getFeed(c *gin.Context) {
	token, err := c.Cookie("token")

	if err != nil {
		log.Println("token not set")
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	userId, err := dbgetUserIdByToken(token)

	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	feed, err := getFeedCache(c.Request.Context(), userId)

	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "user get failure, please try again"})
		return
	}

	if feed == nil {
		if feed, err = getFeedFromDb(userId); err != nil {
			c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "feed get failure, please try again"})
			return
		}

		if err = createFeedCache(c.Request.Context(), userId, feed); err != nil {
			c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "create feed failure, please try again"})
			return
		}
	}

	c.IndentedJSON(http.StatusOK, feed)
}

func getUserByID(c *gin.Context) {
	token, err := c.Cookie("token")

	if err != nil {
		log.Println("token not set")
		return
	}

	if err = dbCheckToken(token); err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	id := c.Param("id")
	user, err := dbReadUser(id)

	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "user get failure, please try again"})
		return
	}

	if user != nil {
		c.IndentedJSON(http.StatusOK, user)
		return
	}

	c.IndentedJSON(http.StatusNotFound, gin.H{"message": "user not found"})
}

func getUserByToken(c *gin.Context) {
	token, err := c.Cookie("token")

	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	userId, err := dbgetUserIdByToken(token)

	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"userId": userId})
}

func createPost(c *gin.Context) {
	token, err := c.Cookie("token")

	if err != nil {
		log.Println("token not set")
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	var post PostModel

	if err := c.BindJSON(&post); err != nil {
		log.Print("model error")
		return
	}

	userId, err := dbgetUserIdByToken(token)

	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	postId, err := dbCreatePost(c.Request.Context(), userId, post.Text)

	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "post failure, please try again"})
		return
	}

	publish(exitCtx, Event{
		Type:        addPosts,
		Posts:       []Post{{PostId: postId, UserId: userId, Text: post.Text}},
		PublisherId: userId})

	c.IndentedJSON(http.StatusOK, gin.H{"id": postId})
}

func deletePost(c *gin.Context) {
	token, err := c.Cookie("token")

	if err != nil {
		log.Println("token not set")
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	idParam := c.Param("id")
	postId, err := strconv.Atoi(idParam)
	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "incorrect post id"})
	}

	userId, err := dbgetUserIdByToken(token)

	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	err = publish(exitCtx, Event{
		Type:        rmPost,
		Posts:       []Post{{PostId: postId}},
		PublisherId: userId})

	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "post removal failure, please try again"})
		return
	}

	err = dbDeletePost(c.Request.Context(), postId, userId)

	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "post removal failure, please try again"})
		return
	}

	c.IndentedJSON(http.StatusOK, gin.H{"message": "post removed"})
}

func updatePost(c *gin.Context) {
	token, err := c.Cookie("token")

	if err != nil {
		log.Println("token not set")
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	userId, err := dbgetUserIdByToken(token)

	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	var post PostModel

	if err := c.BindJSON(&post); err != nil {
		log.Print("model error")
		return
	}

	publish(exitCtx, Event{
		Type:        rmPost,
		Posts:       []Post{{PostId: post.PostId}},
		PublisherId: userId})

	updatedId, err := dbUpdatePost(c.Request.Context(), post.PostId, userId, post.Text)

	if err != nil {
		log.Println("update error", err)
		return
	}

	if updatedId == 0 {
		log.Println("no posts updated")
		return
	}

	err = publish(exitCtx, Event{
		Type:        addPosts,
		Posts:       []Post{{PostId: updatedId, UserId: userId, Text: post.Text}},
		PublisherId: userId})

	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "post update failure, please try again"})
		return
	}

	c.IndentedJSON(http.StatusOK, gin.H{"id": updatedId})
}

func getPost(c *gin.Context) {
	token, err := c.Cookie("token")

	if err != nil {
		log.Println("token not set")
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	err = dbCheckToken(token)

	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	idParam := c.Param("id")
	postId, err := strconv.Atoi(idParam)
	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "incorrect post id"})
	}

	var post Post

	postRef, err := getPostCache(c.Request.Context(), postId)

	if postRef == nil || err != nil {
		log.Print("get post cache error")

		if post, err = dbgetPost(postId); err != nil {
			c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "get post failure, please try again"})
			return
		}

		if err := createPostCache(c.Request.Context(), postId, post); err != nil {
			c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "get post failure, please try again"})
			return
		}
	} else {
		post = *postRef
	}

	c.IndentedJSON(http.StatusOK, gin.H{"post": post})
}

func addFriend(c *gin.Context) {
	token, err := c.Cookie("token")

	if err != nil {
		log.Println("token not set")
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	userId, err := dbgetUserIdByToken(token)

	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	idParam := c.Param("id")
	id, err := strconv.Atoi(idParam)
	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "incorrect user id"})
	}

	if id == userId {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"message": "incorrect user id"})
	}

	posts, err := dbGetUserPosts(id)

	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "get user posts failure, please try again"})
		return
	}

	publish(exitCtx, Event{
		Type:        newFriend,
		Posts:       posts,
		PublisherId: id,
		ConsumerId:  userId})

	err = dbMakeFriends(c.Request.Context(), userId, id)

	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "make friends failure, please try again"})
		return
	}

	c.IndentedJSON(http.StatusOK, gin.H{"message": "success"})
}

func deleteFriend(c *gin.Context) {
	token, err := c.Cookie("token")

	if err != nil {
		log.Println("token not set")
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	userId, err := dbgetUserIdByToken(token)

	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	idParam := c.Param("id")
	id, err := strconv.Atoi(idParam)
	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "incorrect user id"})
	}

	publish(exitCtx, Event{
		Type:        rmFriend,
		PublisherId: id,
		ConsumerId:  userId})

	err = dbRemoveFriend(c.Request.Context(), userId, id)

	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "remove friend failure, please try again"})
		return
	}

	c.IndentedJSON(http.StatusOK, gin.H{"message": "success"})
}

func resetFeedCache(c *gin.Context) {
	token, err := c.Cookie("token")

	if err != nil {
		log.Println("token not set")
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	userId, err := dbgetUserIdByToken(token)

	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	err = removeFeedCache(c.Request.Context(), userId)

	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "reset feed cache failure, please try again"})
		return
	}

	c.IndentedJSON(http.StatusOK, gin.H{"message": "success"})
}

func sendMessage(c *gin.Context) {
	token, err := c.Cookie("token")

	if err != nil {
		log.Println("token not set")
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	var message MessageModel

	if err := c.BindJSON(&message); err != nil {
		log.Print("model error")
		return
	}

	userId, err := dbgetUserIdByToken(token)

	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	idParam := c.Param("userId")
	friendId, err := strconv.Atoi(idParam)
	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "incorrect user id"})
		return
	}

	msgId, err := apiSendMessage(userId, friendId, message.Text)

	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "send message failure, please try again"})
		return
	}

	c.IndentedJSON(http.StatusOK, gin.H{"id": msgId})
}

func listMessages(c *gin.Context) {
	token, err := c.Cookie("token")

	if err != nil {
		log.Println("token not set")
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	userId, err := dbgetUserIdByToken(token)

	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	idParam := c.Param("userId")
	friendId, err := strconv.Atoi(idParam)
	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "incorrect user id"})
		return
	}

	count, err := strconv.Atoi(c.DefaultQuery("count", "100"))
	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "incorrect count"})
		return
	}

	offset, err := strconv.Atoi(c.DefaultQuery("offset", "0"))
	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "incorrect offset"})
		return
	}

	dialog, err := apiListDialog(userId, friendId, offset, count)

	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "get dialog failure, please try again"})
		return
	}

	c.IndentedJSON(http.StatusOK, dialog)
}

func init() {
	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}

	prometheus.Register(httpProcessedRequests)
	prometheus.Register(httpDuration)
	dbInit()
}

func route() {
	defer func() {
		if recover() != nil {
			log.Println("panic recovered")
		}
	}()

	router := gin.Default()

	router.Use(prometheusMiddleware())

	router.GET("/user/:id", getUserByID)
	router.GET("/user", getUserByToken)
	router.POST("/user/register", register)
	router.GET("/user/filter", getUsersByFilter)
	router.POST("/login", login)
	router.GET("/metrics", gin.WrapH(promhttp.Handler()))
	router.GET("/post/feed", getFeed)
	router.POST("/post/create", createPost)
	router.POST("/post/update", updatePost)
	router.POST("/post/delete/:id", deletePost)
	router.GET("/post/get/:id", getPost)
	router.POST("/friend/add/:id", addFriend)
	router.POST("/friend/delete/:id", deleteFriend)
	// if any error occured or the feed refresh has taken too long a client must have a option to reset cache
	router.POST("/post/feed/reset", resetFeedCache)
	router.POST("/dialog/:userId/send", sendMessage)
	router.GET("/dialog/:userId/list", listMessages)

	router.Run(":8080")
}

func main() {
	ctx := context.Background()
	exCtx, exitFunc := context.WithCancel(ctx)
	exitCtx = exCtx
	defer conn.Close()
	defer exitFunc()

	route()
}
