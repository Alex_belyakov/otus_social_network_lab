# Отчёт по лабораторной "Шардирование"

### Init
```
alter system set wal_level = logical;
SELECT run_command_on_workers('alter system set wal_level = logical');
```
docker compose restart

### Sharding
В качестве ключа шардирования был выбран идентификатор пользователя  
Таблица сообщений добавлена как co-located для диалогов пользователя  
В качестве системы был выбран Citus  
```
SELECT create_distributed_table('dialogs', 'userid');
SELECT create_distributed_table('messages', 'userid', colocate_with => 'dialogs'); 
```

### Heavy load users
Для наиболее активных пользователей записи сообщений привязываются к отдельным нодам citus  
Таким образом, все запросы создания и чтения диалогов таких пользователей будут направляться на один шард  
Операция не является блокирующей  
```
SELECT isolate_tenant_to_new_shard('dialogs', <userid>, 'CASCADE');
```

### Resharding without write downtime
Решардинг происходит с использованием указанного числа потоков и в бэкграунде, что позволяет выполнять операцию быстро и не блокировать операции чтения и записи
```
ALTER SYSTEM SET citus.max_background_task_executors_per_node = 2;
SELECT pg_reload_conf();
SELECT citus_rebalance_start();
```