# Отчёт по лабораторной InMemory Базы Данных

### Общие данные
Сравнивались показатели latency и throughput для хранилищ   
  *  postgres 1 instance 20 allowed connections
  *  redis lua scripts 1 instance append only appendfsync always  

### Результат анализа
Для записи и чтения redis показал лучшие результаты в среднем на 20 процентов
Для улучшения результатов можно отключить настройки append only appendfsync always

Для построения графиков latency/throughput использовались запросы  
```
increase(api_http_duration_seconds_sum{job="dialogs", path="api_request_path"}[30s])/increase(api_http_duration_seconds_count{job="dialogs", path="api_request_path"}[30s])  
increase(api_http_requests_total{job="dialogs", path="api_request_path"}[30s])
```

#### list dialog latency  

![latency](https://gitlab.com/Alex_belyakov/otus_social_network_lab/-/raw/80fe7f7f0d77b52d492e44abcdbdc5000f2d83d6/images/dialogs_get_latency.png)

#### list dialog throughput  

![throughput](https://gitlab.com/Alex_belyakov/otus_social_network_lab/-/raw/master/images/dialogs_get_throughput.png?ref_type=heads)

#### add message latency  

![latency](https://gitlab.com/Alex_belyakov/otus_social_network_lab/-/raw/master/images/dialogs_post_latency.png?ref_type=heads)

#### add message throughput  

![throughput](https://gitlab.com/Alex_belyakov/otus_social_network_lab/-/raw/master/images/dialogs_post_throughput.png?ref_type=heads)