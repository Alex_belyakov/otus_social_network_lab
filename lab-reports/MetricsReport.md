# Отчёт по лабораторной "Мониторинг сервиса диалогов"

### 1. Бизнес метрики  
  
Дашборд включает в себя следующие метрики RED  
  * Общее количество отправленных сообщений  
    ```sum(increase(api_sent_messages{job="dialogs"}[30m]))```
  * Среднее количество отправленных сообщений  
  ```avg(increase(api_sent_messages{job="dialogs"}[30m]))```
  * Персентиль 10 % по количеству отправленных сообщений на пользователя  
  ```quantile(0.1, (increase(api_sent_messages{job="dialogs"}[30m])))```
  * Персентиль 80 % по количеству отправленных сообщений на пользователя  
  ```quantile(0.8, (increase(api_sent_messages{job="dialogs"}[30m])))```
  * Количество ошибок пользователей  
  ```increase(api_user_errors{job="dialogs"}[30m])```
  * Среднее время ожидания пользователем ответа сервисов чата  
  ```avg by(job) (increase(api_http_duration_seconds_sum{job="dialogs"}[30m])) / avg by(job) (increase(api_http_duration_seconds_count{job="dialogs"}[30m]))```
  * Время ожидания пользователем ответа сервисов чата персентиль 80 %  
  ```histogram_quantile(0.80, avg(rate(api_http_duration_seconds_bucket[30m])) by (le)) ```  
  

![business](https://gitlab.com/Alex_belyakov/otus_social_network_lab/-/raw/master/images/chat_business_metrics.png?ref_type=heads)
    
### 2. Технические метрики  
  
Дашборд включает в себя следующие метрики. Метрики собраны с помощью Cadvisor + Prometheus  
  * CPU (usage)
  * Memory (usage, swaps, errors)
  * I/O (usage)
  * Network (usage, errors)

![tech](https://gitlab.com/Alex_belyakov/otus_social_network_lab/-/raw/master/images/chat_tech_metrics.png?ref_type=heads)