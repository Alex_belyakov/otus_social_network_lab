# Отчёт по лабораторной "Балансировка нагрузки"

## nginx
  * Были запущены два экземпляра сервиса диалогов
  * nginx настроен на балансировку round-robin между двумя экземплярами диалогов
  * 1000 запросов отправлены с помощью jmeter
  * Во время выполнения запросов отключён KILL -9 инстанс диалогов
  * nginx записал ошибку в логи и перенаправил нагрузку на работающий инстанс
  * все запросы успешно выполнились

### Конфигурация
```
worker_processes  2;  ## Default: 1
worker_rlimit_nofile 8192;

events {
  worker_connections  4096;  ## Default: 1024
}

http {
  index    index.html index.htm;
  default_type application/octet-stream;
  log_format   main '$server_name to: $upstream_addr: [$time_local]  $status '
    '"$request" $body_bytes_sent "$http_referer" '
    '"$http_user_agent" "$http_x_forwarded_for"';
  sendfile     on;
  tcp_nopush   on;
  server_names_hash_bucket_size 128; # this seems to be required for some vhosts

  upstream dialogs {
    server host.docker.internal:5003;
    server host.docker.internal:5005;
  }

  server { 
    listen          80;
    server_name     dialogs;
    access_log      /dev/stdout main;

    location / {
      proxy_pass      http://dialogs;
    }
  }
}
```

### Логи
```
2024-04-21 18:46:43 dialogs to: 192.168.65.2:5005: [21/Apr/2024:15:46:43 +0000]  200 "GET /dialog/2/list HTTP/1.1" 56 "-" "Apache-HttpClient/4.5.14 (Java/1.8.0_391)" "-"
2024-04-21 18:46:43 dialogs to: 192.168.65.2:5005: [21/Apr/2024:15:46:43 +0000]  200 "GET /dialog/2/list HTTP/1.1" 56 "-" "Apache-HttpClient/4.5.14 (Java/1.8.0_391)" "-"
2024-04-21 18:46:43 dialogs to: 192.168.65.2:5003: [21/Apr/2024:15:46:43 +0000]  200 "GET /dialog/2/list HTTP/1.1" 56 "-" "Apache-HttpClient/4.5.14 (Java/1.8.0_391)" "-"
2024-04-21 18:46:43 dialogs to: 192.168.65.2:5003: [21/Apr/2024:15:46:43 +0000]  200 "GET /dialog/2/list HTTP/1.1" 56 "-" "Apache-HttpClient/4.5.14 (Java/1.8.0_391)" "-"
2024-04-21 18:46:43 dialogs to: 192.168.65.2:5003: [21/Apr/2024:15:46:43 +0000]  200 "GET /dialog/2/list HTTP/1.1" 56 "-" "Apache-HttpClient/4.5.14 (Java/1.8.0_391)" "-"
2024-04-21 18:46:43 dialogs to: 192.168.65.2:5003: [21/Apr/2024:15:46:43 +0000]  200 "GET /dialog/2/list HTTP/1.1" 56 "-" "Apache-HttpClient/4.5.14 (Java/1.8.0_391)" "-"  

2024-04-21 18:46:43 2024/04/21 15:46:43 [error] 30#30: *11305 upstream prematurely closed connection while reading response header from upstream, client: 172.23.0.1, server: dialogs, request: "GET /dialog/2/list HTTP/1.1", upstream: "http://192.168.65.2:5005/dialog/2/list", host: "localhost:6000"
2024-04-21 18:46:43 2024/04/21 15:46:43 [error] 30#30: *11423 upstream prematurely closed connection while reading response header from upstream, client: 172.23.0.1, server: dialogs, request: "GET /dialog/2/list HTTP/1.1", upstream: "http://192.168.65.2:5005/dialog/2/list", host: "localhost:6000"

2024-04-21 18:46:46 dialogs to: 192.168.65.2:5003: [21/Apr/2024:15:46:46 +0000]  200 "GET /dialog/2/list HTTP/1.1" 56 "-" "Apache-HttpClient/4.5.14 (Java/1.8.0_391)" "-"
2024-04-21 18:46:46 dialogs to: 192.168.65.2:5003: [21/Apr/2024:15:46:46 +0000]  200 "GET /dialog/2/list HTTP/1.1" 56 "-" "Apache-HttpClient/4.5.14 (Java/1.8.0_391)" "-"
2024-04-21 18:46:46 dialogs to: 192.168.65.2:5003: [21/Apr/2024:15:46:46 +0000]  200 "GET /dialog/2/list HTTP/1.1" 56 "-" "Apache-HttpClient/4.5.14 (Java/1.8.0_391)" "-"
2024-04-21 18:46:46 dialogs to: 192.168.65.2:5003: [21/Apr/2024:15:46:46 +0000]  200 "GET /dialog/2/list HTTP/1.1" 56 "-" "Apache-HttpClient/4.5.14 (Java/1.8.0_391)" "-"
```

## HAProxy
  * Были запущены две асинхронные реплики бд
  * HAProxy настроен на балансировку round-robin между двумя асинхронными репликами
  * 1000 запросов отправлены с помощью jmeter
  * Во время выполнения запросов отключена KILL -9 реплика
  * HAProxy записал ошибку в логи и перенаправил нагрузку на работающую реплику

### Конфигурация

```
global
  log stdout format raw daemon debug

defaults
  mode http
	log global
  timeout connect 5000ms
  timeout client 50000ms
  timeout server 50000ms
	option tcplog
	
listen stats
  mode http
  bind *:7000
  stats enable
  stats uri /
		
listen socialnetworkdb
  mode tcp
	option tcplog
	balance roundrobin
	bind *:5436
	server asyncslave1 host.docker.internal:5433 check
	server asyncslave2 host.docker.internal:5434 check
```

### Логи

```
2024-04-22 10:26:20 172.30.0.1:37554 [22/Apr/2024:07:25:19.556] socialnetworkdb socialnetworkdb/asyncslave1 1/21/61291 19464 cD 5/5/4/1/0 0/0
2024-04-22 10:26:20 172.30.0.1:37566 [22/Apr/2024:07:25:19.583] socialnetworkdb socialnetworkdb/asyncslave1 1/18/61266 19560 cD 4/4/3/0/0 0/0
2024-04-22 10:26:20 172.30.0.1:37582 [22/Apr/2024:07:25:19.588] socialnetworkdb socialnetworkdb/asyncslave2 1/24/61261 19943 cD 3/3/2/1/0 0/0
2024-04-22 10:26:20 172.30.0.1:37546 [22/Apr/2024:07:25:19.556] socialnetworkdb socialnetworkdb/asyncslave2 1/21/61294 19943 cD 2/2/1/1/0 0/0
2024-04-22 10:26:20 172.30.0.1:37564 [22/Apr/2024:07:25:19.582] socialnetworkdb socialnetworkdb/asyncslave2 1/15/61268 19847 cD 1/1/0/0/0 0/0
2024-04-22 10:27:38 172.30.0.1:44682 [22/Apr/2024:07:27:28.728] socialnetworkdb socialnetworkdb/asyncslave1 1/9/9628 16029 -- 5/5/4/2/0 0/0
2024-04-22 10:27:38 172.30.0.1:44670 [22/Apr/2024:07:27:28.717] socialnetworkdb socialnetworkdb/asyncslave1 1/7/9646 15933 -- 4/4/3/1/0 0/0
2024-04-22 10:27:38 172.30.0.1:44664 [22/Apr/2024:07:27:28.714] socialnetworkdb socialnetworkdb/asyncslave1 1/9/9663 15989 -- 4/4/3/0/0 0/0
2024-04-22 10:27:38 172.30.0.1:45094 [22/Apr/2024:07:27:38.646] socialnetworkdb socialnetworkdb/asyncslave1 1/5/18 0 -- 4/4/3/0/0 0/0
2024-04-22 10:27:41 172.30.0.1:45112 [22/Apr/2024:07:27:38.869] socialnetworkdb socialnetworkdb/asyncslave1 1/-1/3066 0 SC 5/5/4/0/3 0/0

2024-04-22 10:27:42 Server socialnetworkdb/asyncslave1 is DOWN, reason: Layer4 connection problem, info: "Connection refused", check duration: 5ms. 1 active and 0 backup servers left. 0 sessions active, 0 requeued, 0 remaining in queue.
2024-04-22 10:27:42 [WARNING] 112/072742 (8) : Server socialnetworkdb/asyncslave1 is DOWN, reason: Layer4 connection problem, info: "Connection refused", check duration: 5ms. 1 active and 0 backup servers left. 0 sessions active, 0 requeued, 0 remaining in queue.

2024-04-22 10:28:30 172.30.0.1:44678 [22/Apr/2024:07:27:28.727] socialnetworkdb socialnetworkdb/asyncslave2 1/8/62169 21095 -- 4/4/3/3/0 0/0
2024-04-22 10:28:30 172.30.0.1:44662 [22/Apr/2024:07:27:28.714] socialnetworkdb socialnetworkdb/asyncslave2 1/8/62183 21191 -- 4/4/2/2/0 0/0
2024-04-22 10:28:31 172.30.0.1:44684 [22/Apr/2024:07:27:38.363] socialnetworkdb socialnetworkdb/asyncslave2 1/21/52879 5255 cD 2/2/1/1/0 0/0
2024-04-22 10:28:31 172.30.0.1:45106 [22/Apr/2024:07:27:38.685] socialnetworkdb socialnetworkdb/asyncslave2 1/6/52563 4679 cD 1/1/0/0/0 0/0

2024-04-22 10:29:57 [WARNING] 112/072957 (8) : Server socialnetworkdb/asyncslave1 is UP, reason: Layer4 check passed, check duration: 6ms. 2 active and 0 backup servers online. 0 sessions requeued, 0 total in queue.
```
