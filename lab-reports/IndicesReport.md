# Отчёт по лабораторной индексы БД

### 0. Общие данные

  * Нагрузка генерировалась с помощью jmeter 10/100/1000 одновременных запросов
  * Сгенерированы 2 000 000 записей в бд
  * Использовался индекс btree, так как текстовые запросы достаточно простые и только на префикс
  * Использование gist и gin несёт дополнительные расходы времени и памяти на перестройку индекса, ограничения по   типам полей, для простых запросов большого выигрыша в поиске по сравнению с btree не дают
    
### 1. Индексы не добавлены

Индекс отсутствует, система сканирует все строки, выполняя Sequential Scan

Запрос
```
explain select name, surname, age, city, interests
from users
where name like 'Б%' and surname like 'Б%'
```

План запроса  
Gather  (cost=1000.00..98012.81 rows=114 width=82)  
Workers Planned: 2"  
Parallel Seq Scan on users  (cost=0.00..97001.41 rows=48 width=82)  
Filter: (((name)::text ~ 'Б%'::text) AND ((surname)::text ~ 'Б%'::text))  


### 2. Добавлен индекс 

Система сканирует индекс, строя битовую карту, так как результат достаточно объёмный, и для поиска в памяти полей, не входящих в индекс, выгоднее использовать bitmap

Индекс
```
CREATE INDEX IF NOT EXISTS idx_name_surname
    ON public.users USING btree
    (name COLLATE pg_catalog."C" ASC NULLS LAST, surname COLLATE pg_catalog."C" ASC NULLS LAST)
    TABLESPACE pg_default;
```

Запрос
```
explain select name, surname, age, city, interests
from users
where name like 'Б%' and surname like 'Б%'
```

План запроса  
Bitmap Heap Scan on users  (cost=425.95..870.01 rows=114 width=82)  
  Filter: (((name)::text ~ 'Б%'::text) AND ((surname)::text ~~ 'Б%'::text))  
Bitmap Index Scan on idx_name_surname  (cost=0.00..425.92 rows=114 width=0)  
  Index Cond: (((name)::text >= 'Б'::text) AND ((name)::text < 'В'::text) AND ((surname)::text >= 'Б'::text) AND ((surname)::text < 'В'::text))

### 3. Графики

Приложены графики для 1000 запросов ramp up = 0, через jmeter получилось около 250 rps  
На графиках видно, что, после добавления индекса количество запросов в минуту увеличилось в три раза  
Средняя продолжительность запросов уменьшилась в 1.5 раза  

Для построения графиков latency/throughput использовались запросы  
```
rate(api_http_requests_total{path="/user"}[1m])
api_http_duration_seconds_sum/api_http_duration_seconds_count {path="/user"}
```
#### latency  

![latency](https://gitlab.com/alexcode1/otus_highload_lab/-/raw/953fefbbe1347caa13f52279ffcf8a27898320fb/images/latency_1000.png)

#### throughput  

![throughput](https://gitlab.com/alexcode1/otus_highload_lab/-/raw/953fefbbe1347caa13f52279ffcf8a27898320fb/images/throughput_1000.png)