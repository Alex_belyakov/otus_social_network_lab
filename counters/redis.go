package main

import (
	"context"
	"fmt"
	"log"
	"os"

	_ "github.com/lib/pq"
	"github.com/redis/go-redis/v9"
)

type DialogCounter struct {
	DialogId int `json:"dialogId"`
	Count    int `json:"count"`
}

func resetDialogMessages(ctx context.Context, receiverId int, dialogId int, messageId int) error {
	redisConnection, exists := os.LookupEnv("redis_connection")

	if !exists {
		log.Fatal("redis connection env not found")
	}
	rdb := redis.NewClient(&redis.Options{
		Addr: redisConnection,
	})

	err := rdb.HSet(ctx, "dialog-read-message-id", fmt.Sprintf("dialog-%d-receiver-%d", dialogId, receiverId), messageId).Err()

	if err != nil {
		log.Println("error resetting the counter", err)
	}

	_, err = rdb.ZRemRangeByScore(ctx, fmt.Sprintf("dialog-%d-receiver-%d", dialogId, receiverId),
		"-inf", fmt.Sprint(messageId)).Result()

	if err != nil {
		log.Println("error resetting the counter", err)
	}

	return err
}

func addDialogMessage(ctx context.Context, receiverId int, dialogId int, messageId int) error {
	redisConnection, exists := os.LookupEnv("redis_connection")

	if !exists {
		log.Fatal("redis connection env not found")
	}
	rdb := redis.NewClient(&redis.Options{
		Addr: redisConnection,
	})

	cmd := rdb.HGet(ctx, "dialog-read-message-id", fmt.Sprintf("dialog-%d-receiver-%d", dialogId, receiverId))
	err := cmd.Err()

	switch err {
	case redis.Nil:
	case nil:
		if lastReadId, err := cmd.Int(); err != nil {
			log.Println("error resetting the counter", err)
			return err
		} else if lastReadId > messageId {
			return nil
		}
	default:
		log.Println("error resetting the counter", err)
		return err
	}

	if err := rdb.ZAdd(ctx, fmt.Sprintf("dialog-%d-receiver-%d", dialogId, receiverId), redis.Z{Score: float64(messageId), Member: messageId}).Err(); err != nil {
		log.Println("error resetting the counter", err)
		return err
	}

	return nil
}

func getDialogCounter(ctx context.Context, receiverId int, dialogId int) (int, error) {
	redisConnection, exists := os.LookupEnv("redis_connection")

	if !exists {
		log.Println("redis connection env not found")
		return 0, fmt.Errorf("redis connection env not found")
	}
	rdb := redis.NewClient(&redis.Options{
		Addr: redisConnection,
	})

	key := fmt.Sprintf("dialog-%d-receiver-%d", dialogId, receiverId)

	if cnt, err := rdb.ZCount(ctx, key, "-inf", "+inf").Result(); err == nil {
		return int(cnt), nil
	} else {
		return 0, err
	}
}

func getUserCounters(ctx context.Context, receiverId int, dialogIds []int) (counts []DialogCounter, err error) {
	for _, id := range dialogIds {
		if count, err := getDialogCounter(ctx, receiverId, id); err == nil {
			counts = append(counts, DialogCounter{Count: count, DialogId: id})
		} else {
			log.Println(err, "get counters error")
		}
	}

	return
}
