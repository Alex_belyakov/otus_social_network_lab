package main

import (
	"context"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	httpProcessedRequests = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "api_http_requests_total",
		Help: "The total number of api http requests",
	}, []string{"path"})
	httpDuration = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name: "api_http_duration_seconds",
		Help: "Duration of HTTP requests.",
	}, []string{"path"})
)

type MessageModel struct {
	Text string `json:"text"`
}

func prometheusMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		path := c.Request.URL.Path
		timer := prometheus.NewTimer(httpDuration.WithLabelValues(path))
		c.Next()

		httpProcessedRequests.WithLabelValues(path).Inc()

		timer.ObserveDuration()
	}
}

func getUserMessageCounts(c *gin.Context) {
	token, err := c.Cookie("token")

	if err != nil {
		log.Println("token not set")
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	userId, err := getUserId(token)

	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	var dialogIds []int
	dialogParams := c.QueryArray("dialogId")
	for _, dParam := range dialogParams {
		dialogId, err := strconv.Atoi(dParam)
		if err != nil {
			c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "incorrect dialog id"})
			return
		}
		dialogIds = append(dialogIds, dialogId)
	}

	cnts, err := getUserCounters(c.Request.Context(), userId, dialogIds)

	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "getting counter failure"})
		return
	}

	c.IndentedJSON(http.StatusOK, gin.H{"counts": cnts})
}

func getUserDialogMessageCounts(c *gin.Context) {
	token, err := c.Cookie("token")

	if err != nil {
		log.Println("token not set")
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	userId, err := getUserId(token)

	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	dialogParam := c.Param("dialogId")
	dialogId, err := strconv.Atoi(dialogParam)
	if err != nil {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "incorrect dialog id"})
		return
	}

	cnt, err := getDialogCounter(c.Request.Context(), userId, dialogId)

	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "getting counter failure"})
		return
	}

	c.IndentedJSON(http.StatusOK, gin.H{"count": cnt})
}

func init() {
	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}

	prometheus.Register(httpProcessedRequests)
	prometheus.Register(httpDuration)
}

func route() {
	defer func() {
		if recover() != nil {
			log.Println("panic recovered")
		}
	}()

	router := gin.Default()

	router.Use(prometheusMiddleware())

	router.GET("/metrics", gin.WrapH(promhttp.Handler()))
	router.GET("/dialogs/:dialogId/message/count", getUserDialogMessageCounts)
	router.GET("dialogs/message/count", getUserMessageCounts)

	router.Run(":80")
}

func main() {
	ctx := context.Background()
	cancelCtx, cancelFunc := context.WithCancel(ctx)
	defer cancelFunc()

	if err := startEventsListener(cancelCtx); err != nil {
		panic(err)
	}

	route()
}
