package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

type UserProfile struct {
	UserId int `json:"userId"`
}

func getUserId(token string) (userId int, err error) {
	baseurl, exists := os.LookupEnv("auth_api_baseurl")
	if !exists {
		log.Println("api base url env not found")
		return
	}

	req, err := http.NewRequest("GET", baseurl+"user", nil)
	if err != nil {
		log.Println("api error", err)
		return
	}

	req.Header.Set("Accept", "application/json")
	req.AddCookie(&http.Cookie{Name: "token", Value: token})

	client := &http.Client{}
	resp, err := client.Do(req)

	if err != nil {
		fmt.Printf("error making http request: %s\n", err)
		return
	}

	fmt.Printf("client: status code: %d\n", resp.StatusCode)

	if resp.StatusCode == 200 {
		defer resp.Body.Close()

		b, err := io.ReadAll(resp.Body)

		if err == nil {
			var user UserProfile
			if err = json.Unmarshal(b, &user); err == nil {
				userId = user.UserId
			}
		}
	}

	return
}
