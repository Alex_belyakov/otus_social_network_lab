package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/IBM/sarama"
	_ "github.com/lib/pq"
	"golang.org/x/exp/constraints"
)

const (
	sendEvent int = 1
	readEvent int = 2
)

type Event struct {
	Id         int       `json:"id"`
	Type       int       `json:"type"`
	SenderId   int       `json:"senderId"`
	ReceiverId int       `json:"receiverId"`
	DialogId   int       `json:"dialogId"`
	Timestamp  time.Time `json:"timestamp"`
	MessageIds []int     `json:"messageIds"`
}

type ConfirmEvent struct {
	Id int `json:"id"`
}

func startEventsListener(ctx context.Context) (err error) {
	kafkaConnStr, exists := os.LookupEnv("kafka_connection")

	if !exists {
		log.Println("kafka connection env not found")
		return
	}

	kafkaConsumers, exists := os.LookupEnv("kafka_consumers")

	if !exists {
		log.Println("kafka consumers env not found")
		return
	}

	consumersCnt, err := strconv.Atoi(kafkaConsumers)
	if err != nil {
		log.Println("incorrect kafka consumers env")
		return
	}

	brokerAddrs := []string{kafkaConnStr}
	config := sarama.NewConfig()
	config.Version = sarama.V2_1_0_0
	admin, err := sarama.NewClusterAdmin(brokerAddrs, config)
	if err != nil {
		log.Fatal("Error while creating cluster admin: ", err.Error())
	}
	defer func() { _ = admin.Close() }()

	if topics, err := admin.ListTopics(); err == nil {
		if _, ok := topics["chat-events"]; !ok {
			err = admin.CreateTopic("chat-events", &sarama.TopicDetail{
				NumPartitions:     int32(consumersCnt),
				ReplicationFactor: -1,
			}, false)
			if err != nil {
				log.Fatal("Error while creating topic: ", err.Error())
			}
		}
	} else {
		log.Fatal("Error while creating topic: ", err.Error())
	}

	consumer, err := sarama.NewConsumer([]string{kafkaConnStr}, nil)
	if err != nil {
		log.Println("Failed to create consumer", err)
		return
	}

	for i := 0; i < consumersCnt; i++ {
		partConsumer, errConsumer := consumer.ConsumePartition("chat-events", int32(i), sarama.OffsetNewest)
		if errConsumer != nil {
			log.Println("Failed to consume partition", err)
			return
		}

		go func() {
			// Создание продюсера Kafka
			producer, err := sarama.NewSyncProducer([]string{kafkaConnStr}, nil)
			if err != nil {
				log.Fatalf("Failed to create producer: %v", err)
			}

			defer producer.Close()
			defer partConsumer.Close()
			defer consumer.Close()

			for {
				select {
				case msg, ok := <-partConsumer.Messages():
					if !ok {
						log.Println("Channel closed, exiting goroutine")
						return
					}

					var event Event
					err := json.Unmarshal(msg.Value, &event)

					if err != nil {
						log.Println("Error unmarshaling JSON:", err)
						continue
					}

					log.Printf("Received message: %+v\n", event)

					switch event.Type {
					case sendEvent:
						if len(event.MessageIds) > 0 {
							err = addDialogMessage(ctx, event.ReceiverId, event.DialogId, event.MessageIds[0])
						} else {
							log.Println(err, "no ids in the event")
						}
					case readEvent:
						var maxId int

						if maxId, err = max(event.MessageIds); err != nil {
							log.Println(err, "error when responding to an event")
							continue
						}
						err = resetDialogMessages(ctx, event.ReceiverId, event.DialogId, maxId)
					default:
						log.Println("not known event")
					}

					if err == nil {
						err = respond(producer, event)
					}

					if err != nil {
						log.Println(err, "error when responding to an event")
					}
				case <-ctx.Done():
					return
				}
			}
		}()
	}

	return
}

func max[T constraints.Ordered](slice []T) (max T, err error) {
	if len(slice) == 0 {
		err = fmt.Errorf("empty slice")
		return
	}

	if len(slice) == 1 {
		return slice[0], nil
	}

	max = slice[0]

	for _, t := range slice {
		if t > max {
			max = t
		}
	}

	return
}

func respond(producer sarama.SyncProducer, event Event) (err error) {
	if response, err := json.Marshal(ConfirmEvent{Id: event.Id}); err == nil {
		msg := &sarama.ProducerMessage{
			Topic: "chat-event-replies",
			Key:   sarama.StringEncoder(fmt.Sprint(event.Id)),
			Value: sarama.ByteEncoder(response),
		}

		// отправка сообщения в Kafka
		if _, _, err = producer.SendMessage(msg); err != nil {
			log.Println("Failed to send message to Kafka", err)
		}
	} else {
		log.Println("Failed to send message to Kafka", err)
	}

	return
}
