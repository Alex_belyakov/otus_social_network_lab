package main

import (
	"context"
	"encoding/json"
	"log"
	"os"

	"github.com/IBM/sarama"
	_ "github.com/lib/pq"
)

type Post struct {
	UserId int    `json:"userId"`
	PostId int    `json:"postId"`
	Text   string `json:"text"`
}

type PostChannelEvent struct {
	Post      Post  `json:"post"`
	FriendIds []int `json:"friendIds"`
}

func startEventsListener(ctx context.Context) (err error) {
	kafkaConnStr, exists := os.LookupEnv("kafka_connection")

	if !exists {
		log.Println("kafka connection env not found")
		return
	}
	consumer, err := sarama.NewConsumer([]string{kafkaConnStr}, nil)
	if err != nil {
		log.Println("Failed to create consumer", err)
		return
	}

	partConsumer, err := consumer.ConsumePartition("post-feed", 0, sarama.OffsetNewest)
	if err != nil {
		log.Println("Failed to consume partition", err)
		return
	}

	go func() {
		defer partConsumer.Close()
		defer consumer.Close()

		for {
			select {
			case <-ctx.Done():
				return
			case msg, ok := <-partConsumer.Messages():
				if !ok {
					log.Println("Channel closed, exiting goroutine")
					return
				}

				var event PostChannelEvent
				err := json.Unmarshal(msg.Value, &event)

				if err != nil {
					log.Println("Error unmarshaling JSON:", err)
					continue
				}

				log.Printf("Received message: %+v\n", event)

				if err = handlePostAdded(event); err != nil {
					log.Println("Error handling post updates", err)
				}
			}
		}
	}()

	return
}

func handlePostAdded(event PostChannelEvent) (err error) {
	for _, id := range event.FriendIds {
		if channel, ok := getChan(id); ok {
			channel <- event.Post
		}
	}

	return
}
