package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"sync"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	chans    map[int]chan Post = make(map[int]chan Post)
	mu       sync.RWMutex
	upgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}
	httpProcessedRequests = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "api_http_requests_total",
		Help: "The total number of api http requests",
	}, []string{"path"})
	httpDuration = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name: "api_http_duration_seconds",
		Help: "Duration of HTTP requests.",
	}, []string{"path"})
	conn    *sql.DB
	exitCtx context.Context
)

const (
	startListening = 1
)

type CommandMessage struct {
	Command int `json:"command"`
}

func prometheusMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		path := c.Request.URL.Path
		timer := prometheus.NewTimer(httpDuration.WithLabelValues(path))
		c.Next()

		httpProcessedRequests.WithLabelValues(path).Inc()

		timer.ObserveDuration()
	}
}

func connect(c *gin.Context) {
	token := c.Param("token")

	userId, err := apiGetUserId(token)

	if err != nil || userId == 0 {
		c.IndentedJSON(http.StatusUnauthorized, gin.H{"message": "user not authorized"})
		return
	}

	maxConnections, exists := os.LookupEnv("max_connections")

	if !exists || maxConnections == fmt.Sprintf("%d", len(chans)) {
		log.Println("kafka connection env not found")
		c.IndentedJSON(http.StatusBadRequest, gin.H{"message": "too many connections"})
		return
	}

	ws, err := upgrader.Upgrade(c.Writer, c.Request, nil)

	if err != nil {
		log.Println(err)
	}

	go read(userId, ws)
}

func read(userId int, ws *websocket.Conn) {
	defer ws.Close()

	for {
		select {
		case <-exitCtx.Done():
			return
		default:
			_, msgTxt, err := ws.ReadMessage()

			if err != nil {
				closeChan(userId)
				return
			}
			var message CommandMessage

			if err = json.Unmarshal(msgTxt, &message); err != nil {
				log.Println("cannot unmarshal the message", err)
				continue
			}

			switch message.Command {
			case startListening:
				addChan(userId)
				go startWriting(userId, chans[userId], ws)
			default:
				fmt.Println("command cannot be interpreted")
			}
		}
	}
}

func getChan(userId int) (channel chan Post, ok bool) {
	channel, ok = chans[userId]

	return
}

func startWriting(userId int, posts chan Post, socket *websocket.Conn) {
	for {
		select {
		case <-exitCtx.Done():
			return
		case p, ok := <-posts:
			if !ok {
				fmt.Println("chan is closed")
				return
			}

			if msg, err := json.Marshal(p); err == nil {
				if err := socket.WriteMessage(websocket.TextMessage, msg); err != nil {
					fmt.Println("web socker error", err)
					closeChan(userId)
					return
				}
			}
		}
	}
}

func closeChan(userId int) {
	mu.Lock()
	defer mu.Unlock()
	if _, ok := chans[userId]; ok {
		close(chans[userId])
		delete(chans, userId)
	}
}

func addChan(userId int) {
	mu.Lock()
	defer mu.Unlock()
	chans[userId] = make(chan Post)
}

func init() {
	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}

	prometheus.Register(httpProcessedRequests)
	prometheus.Register(httpDuration)
}

func route() {
	defer func() {
		if recover() != nil {
			log.Println("panic recovered")
		}
	}()

	router := gin.Default()

	router.Use(prometheusMiddleware())

	router.GET("/ws/:token", connect)
	router.GET("/metrics", gin.WrapH(promhttp.Handler()))

	router.Run(":80")
}

func main() {
	ctx := context.Background()
	exCtx, exitFunc := context.WithCancel(ctx)
	exitCtx = exCtx
	defer conn.Close()
	defer exitFunc()

	if err := startEventsListener(exitCtx); err != nil {
		panic("posts topic unaccessible")
	}

	route()
}
